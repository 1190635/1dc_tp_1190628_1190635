/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Date;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author game
 */
public class CandidaturaTest {
    
    public CandidaturaTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }



    /**
     * Test of calcularMedia method, of class Candidatura.
     */
    @Test
    public void testCalcularMedia() {
        System.out.println("calcularMedia");
        Tarefa tarefa= new Tarefa("ref","designacao","desInf","desTec",20,1200);
        CompetenciaTecnica competenciaTecnica = new CompetenciaTecnica("1","descBreve","descDetalhada");
        Reconhecimento rec = new Reconhecimento(new Date(2020-1900,5-1,20),competenciaTecnica,GrauProficiencia.ALTO);
        CategoriaTarefa categoriaTarefa= new CategoriaTarefa("id","descricao");
        CaraterCT caraterCT= new CaraterCT(true,GrauProficiencia.MEDIO,competenciaTecnica);
        categoriaTarefa.addCaraterCt(caraterCT);
        tarefa.setCategoriaTarefa(categoriaTarefa);
        Freelancer freelancer= new Freelancer("Carvalho","100000000","100000000","carvalho@gmai.com");
        freelancer.addReconhecimento(rec);
        Candidatura candidatura1 = new Candidatura(new Date(2020-1900,5-1,22),1000,10,"txtApresentacao","txtMotivacao",freelancer);
        double result = candidatura1.calcularMediaNP(tarefa);
        double expResult=4;
        assertEquals(expResult, result);




    }



    /**
     * Test of calcularDesvioPadrao method, of class Candidatura.
     */
    @Test
    public void testCalcularDesvioPadrao() {
        System.out.println("calcularDesvioPadrao");
        Tarefa tarefa= new Tarefa("ref","designacao","desInf","desTec",20,1200);
        CompetenciaTecnica competenciaTecnica = new CompetenciaTecnica("1","descBreve","descDetalhada");
        Reconhecimento rec = new Reconhecimento(new Date(2020-1900,5-1,20),competenciaTecnica,GrauProficiencia.ALTO);
        CategoriaTarefa categoriaTarefa= new CategoriaTarefa("id","descricao");
        CaraterCT caraterCT= new CaraterCT(true,GrauProficiencia.MEDIO,competenciaTecnica);
        categoriaTarefa.addCaraterCt(caraterCT);
        tarefa.setCategoriaTarefa(categoriaTarefa);
        Freelancer freelancer= new Freelancer("Carvalho","100000000","100000000","carvalho@gail.com");
        freelancer.addReconhecimento(rec);
        Candidatura candidatura1 = new Candidatura(new Date(2020-1900,5-1,22),1000,10,"txtApresentacao","txtMotivacao",freelancer);
        double result = candidatura1.calcularDesvioPadrao(tarefa);
        double expResult=0;
        assertEquals(expResult, result);

    }

    
}
