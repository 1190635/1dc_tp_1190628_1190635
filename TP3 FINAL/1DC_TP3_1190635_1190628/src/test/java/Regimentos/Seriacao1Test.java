/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regimentos;

import Model.Candidatura;
import Model.CaraterCT;
import Model.CategoriaTarefa;
import Model.CompetenciaTecnica;
import Model.Freelancer;
import Model.GrauProficiencia;
import Model.Reconhecimento;
import Model.Tarefa;
import java.util.Date;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ptubb
 */
public class Seriacao1Test {
    
    public Seriacao1Test() {
    }

    /**
     * Test of compare method, of class Seriacao1.
     */
    @org.junit.Test
    public void testCompare() {
           System.out.println("compare");
        Tarefa tarefa= new Tarefa("ref","designacao","desInf","desTec",20,1200);
        CompetenciaTecnica competenciaTecnica = new CompetenciaTecnica("1","descBreve","descDetalhada");
        Reconhecimento rec = new Reconhecimento(new Date(2020-1900,5-1,20),competenciaTecnica,GrauProficiencia.ALTO);
        CategoriaTarefa categoriaTarefa= new CategoriaTarefa("id","descricao");
        CaraterCT caraterCT= new CaraterCT(true,GrauProficiencia.MEDIO,competenciaTecnica);
        categoriaTarefa.addCaraterCt(caraterCT);
        tarefa.setCategoriaTarefa(categoriaTarefa);
        Freelancer freelancer= new Freelancer("Carvalho","100000000","100000000","carvalho@gmail.com");
        freelancer.addReconhecimento(rec);
        Candidatura candidatura1 = new Candidatura(new Date(2020-1900,5-1,22),1000,10,"txtApresentacao","txtMotivacao",freelancer);
        candidatura1.calcularMediaNP(tarefa);

        Reconhecimento rec2 = new Reconhecimento(new Date(2020-1900,5-1,20),competenciaTecnica,GrauProficiencia.MEDIO);
        Freelancer freelancer2= new Freelancer("Goncalo","100000001","100000001","goncalo@gmail.com");
        freelancer.addReconhecimento(rec2);
        Candidatura candidatura2 = new Candidatura(new Date(2020-1900,5-1,22),1000,10,"txtApresentacao","txtMotivacao",freelancer);
        candidatura2.calcularMediaNP(tarefa);


        Seriacao1 instance = new Seriacao1();
        int expResult = -1;
        int result = instance.compare(candidatura1, candidatura2);
        assertEquals(expResult, result);

    }
    
}
