package Controller;


import Model.Anuncio;
import Model.Candidatura;
import Model.Plataforma;
import Model.ProcessoSeriacao;
import Model.Startup;
import java.io.FileNotFoundException;

import java.util.List;
import utils.*;
import utils.ListaCandidaturas;
import utils.ListaTarefas;
import utils.RegistoAnuncios;

public class SeriarAnuncioController {

    /**
     * objeto plataforma da classe Plataforma
     */
    private Plataforma plataforma;

    /**
     * objeto anu da classe Anuncio
     */
    private Anuncio anu;

    /**
     * objeto cand da classe Candidatura
     */
    private Candidatura cand;

    /**
     * objeto ps da classe ProcessoSeriacao
     */
    private ProcessoSeriacao ps;

    /**
     * objeto listaCand da classe ListaCandidaturas
     */
    private ListaCandidaturas listaCand;

    /**
     * objeto ra da classe RegistoAnuncios
     */
    private RegistoAnuncios ra;

    private ListaTarefas listaTarefas;

    /**
     * Metodo de confirmacao do utilizador
     */
    public SeriarAnuncioController() throws FileNotFoundException {
        plataforma = new Plataforma("plataforma");

        LerCompetenciasTecnicas lerCompetenciasTecnicas = new LerCompetenciasTecnicas();
        plataforma.getRegistoCompetenciaTecnica().setCompetenciasTecnicas(lerCompetenciasTecnicas.lerCompetencias());

        LerTarefas lerTarefas = new LerTarefas();

        plataforma.getRegistoCategoria().setListaCategorias(lerTarefas.lerCategorias(plataforma));

        listaTarefas = new ListaTarefas(lerTarefas.lerTarefas(plataforma));





//        LerAnuncios lerAnuncios = new LerAnuncios();
//        plataforma.getRegistoAnuncios().setAnuncios(LerAnuncios.ler(listaTarefas.getTarefas()));
//
//        LerFreelancers lerFreelancers = new LerFreelancers();
//        plataforma.getRegistoFreelancer().setFreelancers(lerFreelancers.ler_criarFreelancers(plataforma));
//        lerFreelancers.ler_guardarReconhecimento(plataforma);
//
//        LerCandidaturas lerCandidaturas = new LerCandidaturas();
//        lerCandidaturas.lerCandidaturas(plataforma);

    }


    public List<Anuncio> getListaAnuncios(){
        return plataforma.getRegistoAnuncios().getAnuncios();
    }

    /**
     * Metodo de acesso dos anuncios
     */
    public List<Anuncio> getAnuncios() {
        return plataforma.getRegistoAnuncios().getAnuncios();
    }

    /**
     * Metodo de acesso das candidaturas
     */
    public List<Candidatura> getCandidaturas() {
        this.listaCand = anu.getListaCandidaturas();

        return listaCand.getCandidaturas();
    }

    /**
     * inicia o processo de seriacao
     */
    public void iniciarSeriacao() {
        // this.cand = listaCand.getCandidatura(candId); // é necessario id?
        // ps.addClassificacao(cand,ordem);
    }

    /**
     * Regista o processo de seriacao
     */
    public void registaProcessoSeriacao() {
        anu.registaProcessoSeriacao(ps);
    }
}
