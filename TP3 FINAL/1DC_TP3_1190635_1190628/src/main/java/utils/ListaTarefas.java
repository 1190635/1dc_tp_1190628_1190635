/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.Tarefa;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class ListaTarefas {

    List<Tarefa> lstTarefas;

    public ListaTarefas(List<Tarefa> tarefas) {
        this.lstTarefas = tarefas;
    }

    public ListaTarefas() {
        lstTarefas = new ArrayList<Tarefa>();
    }

    public List<Tarefa> getTarefas() {
        return lstTarefas;
    }

    public void setTarefas(List<Tarefa> tarefas) {
        this.lstTarefas = tarefas;
    }

    public boolean addTarefa(Tarefa tarefa) {
        return lstTarefas.add(tarefa);
    }

    public boolean validaTarefa(Tarefa tarefa) {
        return !lstTarefas.contains(tarefa);
    }
}
