/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.Anuncio;
import Model.Candidatura;
import Model.Freelancer;
import Model.Plataforma;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ptubb
 */
public class LerCandidaturas {

    public void lerCandidaturas(Plataforma plataforma) throws FileNotFoundException {
        final String file = "src/main/resources/ClassFiles/ListaCandidaturas.txt";
        Scanner ler = new Scanner(new File(file));
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[;]");
            String[] data = itens[0].split("[/]");
            int ano1 = Integer.parseInt(data[0]);
            int mes1 = Integer.parseInt(data[1]);
            int dia1 = Integer.parseInt(data[2]);
            Date date1 = new Date(ano1, mes1, dia1);
            Anuncio anuncio = procurarAnuncio(plataforma, itens[6]);
            Freelancer freelancer = procurarFreelancer(plataforma, itens[5]);
            anuncio.getListaCandidaturas().addCandidatura(new Candidatura(date1, Double.parseDouble(itens[3]), Integer.parseInt(itens[4]), itens[1], itens[2], freelancer));
        }
    }

    public Freelancer procurarFreelancer(Plataforma plataforma, String NIF) {
        RegistoFreelancers rf = plataforma.getRegistoFreelancer();
        List<Freelancer> lista = rf.getListaFreelancers();
        for (Freelancer freelancer : lista) {
            if (freelancer.getNIF().equals(NIF)) {
                return freelancer;
            }
        }
        return null;
    }

    public Anuncio procurarAnuncio(Plataforma plataforma, String ID) {
        RegistoAnuncios ra = plataforma.getRegistoAnuncios();
        List<Anuncio> lista = ra.getAnuncios();
        for (Anuncio anuncio : lista) {
            if (anuncio.getAnuncioID().equals(ID)) {
                return anuncio;
            }
        }
        return null;
    }
}
