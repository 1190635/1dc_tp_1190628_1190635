/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.TipoRegimento;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class RegistoTiposRegimento {

    List<TipoRegimento> tipoRegimentos;

    public void setTipoRegimentos() {
        this.tipoRegimentos = new ArrayList<TipoRegimento>();
    }

    public void setTipoRegimentos(List<TipoRegimento> tipoRegimentos) {
        this.tipoRegimentos = tipoRegimentos;
    }

    public boolean addTipoRegimento(TipoRegimento tipoRegimento) {
        return tipoRegimentos.add(tipoRegimento);
    }

    public boolean validaTipoRegimento(TipoRegimento tipoRegimento) {
        return tipoRegimentos.contains(tipoRegimento);
    }
}
