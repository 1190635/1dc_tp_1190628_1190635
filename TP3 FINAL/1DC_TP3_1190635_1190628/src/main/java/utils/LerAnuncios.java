/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.Anuncio;
import Model.Plataforma;
import Model.Tarefa;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Gonçalo
 */
public class LerAnuncios {

    public static List<Anuncio> ler(List<Tarefa> listaTarefas) throws FileNotFoundException {
        final String FILE = "src/main/resources/ClassFiles/ListaAnuncios.txt";
        Scanner ler = new Scanner(new File(FILE));
        List<Anuncio> lstAnuncios = new ArrayList<Anuncio>();
        int i = 0;
        while (ler.hasNextLine() && i < listaTarefas.size()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[/;]");
            int ano1 = Integer.parseInt(itens[0]);
            int mes1 = Integer.parseInt(itens[1]);
            int dia1 = Integer.parseInt(itens[2]);

            int ano2 = Integer.parseInt(itens[3]);
            int mes2 = Integer.parseInt(itens[4]);
            int dia2 = Integer.parseInt(itens[5]);

            int ano3 = Integer.parseInt(itens[6]);
            int mes3 = Integer.parseInt(itens[7]);
            int dia3 = Integer.parseInt(itens[8]);

            int ano4 = Integer.parseInt(itens[9]);
            int mes4 = Integer.parseInt(itens[10]);
            int dia4 = Integer.parseInt(itens[11]);

            int ano5 = Integer.parseInt(itens[12]);
            int mes5 = Integer.parseInt(itens[13]);
            int dia5 = Integer.parseInt(itens[14]);

            int ano6 = Integer.parseInt(itens[15]);
            int mes6 = Integer.parseInt(itens[16]);
            int dia6 = Integer.parseInt(itens[17]);

            Date date1 = new Date(ano1, mes1, dia1);
            Date date2 = new Date(ano2, mes2, dia2);
            Date date3 = new Date(ano3, mes3, dia3);
            Date date4 = new Date(ano4, mes4, dia4);
            Date date5 = new Date(ano5, mes5, dia5);
            Date date6 = new Date(ano6, mes6, dia6);

            Tarefa tarefa = listaTarefas.get(i);
            lstAnuncios.add(new Anuncio(tarefa, date1, date2, date3, date4, date5, date6, itens[18]));
            i++;
        }
        ler.close();
        return lstAnuncios;
    }
}
