/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.CompetenciaTecnica;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ptubb
 */
public class LerCompetenciasTecnicas {

    public List<CompetenciaTecnica> lerCompetencias() throws FileNotFoundException {
        final String file = "src/main/resources/ClassFiles/ListaCompetencias.txt";
        Scanner ler = new Scanner(new File(file));
        List<CompetenciaTecnica> listaCompetencias = new ArrayList<CompetenciaTecnica>();
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[;]");
            listaCompetencias.add(new CompetenciaTecnica(itens[0], itens[1], itens[2]));
        }
        ler.close();
        return listaCompetencias;
    }
}
