/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.CategoriaTarefa;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class RegistoCategorias {

    private List<CategoriaTarefa> lstCategorias;

    public RegistoCategorias() {
        lstCategorias = new ArrayList<CategoriaTarefa>();
    }

    public RegistoCategorias(List<CategoriaTarefa> registoCategorias) {
        this.lstCategorias = registoCategorias;
    }

    public List<CategoriaTarefa> getlistaCategorias() {
        return lstCategorias;
    }

    public void setListaCategorias(List<CategoriaTarefa> listaCategorias) {
        this.lstCategorias = listaCategorias;
    }

    public boolean addCategoria(CategoriaTarefa categoria) {
        return lstCategorias.add(categoria);
    }

    public boolean validaCategoria(CategoriaTarefa categoria) {
        return !lstCategorias.contains(categoria);
    }
}
