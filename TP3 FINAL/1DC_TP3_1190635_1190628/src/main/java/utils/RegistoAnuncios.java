/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.Anuncio;
import Model.Tarefa;
import Model.TipoRegimento;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class RegistoAnuncios {

    List<Anuncio> anuncios;

    public RegistoAnuncios(List<Anuncio> anuncios) {
        this.anuncios = anuncios;
    }

    public RegistoAnuncios() {
        anuncios = new ArrayList<Anuncio>();
    }

    public Anuncio getAnuncioByID(String anuncioID) {
        return anuncios.get(Integer.parseInt(anuncioID) - 1);
    }

    public List<Anuncio> getAnuncios() {
        return anuncios;
    }

    public void setAnuncios(List<Anuncio> anuncios) {
        this.anuncios = anuncios;
    }

    public boolean addAnuncio(Anuncio anuncio) {
        return anuncios.add(anuncio);
    }

    public boolean validaAnuncio(Anuncio anuncio) {
        return !anuncios.contains(anuncio);
    }

    public Anuncio novoAnuncio(Tarefa tarefa, Date dtInicioPublicitacao, Date dtFimPublicitacao, Date dtIncioCandidatura, Date dtFimCandidatura, Date dtInicioSeriacao, Date dtFimSeriacao, TipoRegimento tipoRegimento, String ID) {
        return new Anuncio(tarefa, dtInicioPublicitacao, dtFimPublicitacao, dtIncioCandidatura, dtFimCandidatura, dtInicioSeriacao, dtFimSeriacao, ID);
    }
}
