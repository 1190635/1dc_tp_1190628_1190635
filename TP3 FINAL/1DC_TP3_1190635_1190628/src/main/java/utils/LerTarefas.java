/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.CaraterCT;
import Model.CategoriaTarefa;
import Model.CompetenciaTecnica;
import Model.GrauProficiencia;
import Model.Plataforma;
import Model.Tarefa;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ptubb
 */
public class LerTarefas {
    public List<Tarefa> lerTarefas(Plataforma plataforma) throws FileNotFoundException {
        final String FILE = "src/main/resources/ClassFiles/ListaTarefas.txt";
        Scanner ler = new Scanner(new File(FILE));
        List<Tarefa> listaTarefas = new ArrayList<Tarefa>();
        adicionarCaracter(plataforma);
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[;]");
            Tarefa tarefa = new Tarefa(itens[0], itens[1], itens[2], itens[3], Integer.parseInt(itens[4]),Double.parseDouble(itens[5]));
            tarefa.setCategoriaTarefa(procurarCategoria(plataforma, itens[6]));
            listaTarefas.add(tarefa);
        }
        return listaTarefas;
    }

 
    public List<CategoriaTarefa> lerCategorias(Plataforma plataforma) throws FileNotFoundException {
        final String FILE = "src/main/resources/ClassFiles/ListaCategorias.txt";
        Scanner ler = new Scanner(new File(FILE));
        List<CategoriaTarefa> listaCategorias = new ArrayList<CategoriaTarefa>();
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[;]");
            listaCategorias.add(new CategoriaTarefa(itens[0], itens[1]));
        }
        return listaCategorias;
    }

    public void adicionarCaracter(Plataforma plataforma) throws FileNotFoundException {
        final String FILE = "src/main/resources/ClassFiles/ListaCaracter.txt";
        Scanner ler = new Scanner(new File(FILE));
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[;]");
            CategoriaTarefa categoriaTarefa = procurarCategoria(plataforma, itens[0]);

            boolean obrigatoria = Boolean.parseBoolean(itens[1]);
            CompetenciaTecnica ct = procurarCompetenciaTecnica(plataforma, itens[2]);
            GrauProficiencia grauMinimo = procurarEnum(itens[3]);
            CaraterCT ctt = new CaraterCT(obrigatoria, grauMinimo, ct);
            categoriaTarefa.addCaraterCt(ctt);
        }
    }

    public CategoriaTarefa procurarCategoria(Plataforma plataforma, String idCategoria) {
        RegistoCategorias rc = plataforma.getRegistoCategoria();
        List<CategoriaTarefa> lista = rc.getlistaCategorias();
        for (CategoriaTarefa ct : lista) {
            if (ct.getId().equals(idCategoria)) {
                return ct;
            }
        }
        return null;
    }

    public CompetenciaTecnica procurarCompetenciaTecnica(Plataforma plataforma, String cod) {
        RegistoCompetenciasTecnicas registoCompetencia = plataforma.getRegistoCompetenciaTecnica();
        List<CompetenciaTecnica> lista = registoCompetencia.getCompetenciaTecnicas();
        for (CompetenciaTecnica ct : lista) {
            if (ct.getCodigo().equalsIgnoreCase(cod)) {
                return ct;
            }
        }
        return null;
    }

public GrauProficiencia procurarEnum(String string) {
        for (GrauProficiencia grp : GrauProficiencia.values()) {
            if (grp.name().equalsIgnoreCase(string)) {
                return grp;
            }
        }
        return null;
    }
}
