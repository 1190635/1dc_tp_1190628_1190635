/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.CompetenciaTecnica;
import Model.Freelancer;
import Model.GrauProficiencia;
import Model.Plataforma;
import Model.Reconhecimento;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author ptubb
 */
public class LerFreelancers {

    final String FILE = "src/main/resources/ClassFiles/Freelancers.txt";
    final String FILE_REC = "src/main/resources/ClassFiles/ListaReconhecimentos.txt";

    public List<Freelancer> ler_criarFreelancers(Plataforma plataforma) throws FileNotFoundException {
        Scanner ler = new Scanner(new File(FILE));
        List<Freelancer> freelancers = new ArrayList<Freelancer>();
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split(";");
            freelancers.add(new Freelancer(itens[0], itens[1], itens[2], itens[3]));
        }
        ler.close();
        return freelancers;
    }

    public void ler_guardarReconhecimento(Plataforma plataforma) throws FileNotFoundException {
        Scanner ler = new Scanner(new File(FILE_REC));
        while (ler.hasNextLine()) {
            String linha = ler.nextLine();
            String[] itens = linha.split("[/;]");
            Freelancer freelancer = getFreelancerbyNIF(plataforma, itens[0]);
            int ano = Integer.parseInt(itens[1]);
            int mes = Integer.parseInt(itens[2]);
            int dia = Integer.parseInt(itens[3]);
//            freelancer.addReconhecimento(new Reconhecimento(new Date(ano, mes, dia), procurarCompetenciaTecnica(plataforma, itens[4]), procurarEnum(itens[5])));
        }
        ler.close();
    }

    public Freelancer getFreelancerbyNIF(Plataforma plataforma, String NIF) {
        Freelancer freelancer = new Freelancer();
        RegistoFreelancers rf = plataforma.getRegistoFreelancer();
        List<Freelancer> freelancers = rf.getListaFreelancers();

        for (Freelancer f : freelancers) {
            if (f.getNIF().equalsIgnoreCase(NIF)) {
                freelancer = f;
            }
        }
        return freelancer;
    }

    public CompetenciaTecnica procurarCompetenciaTecnica(Plataforma plataforma, String cod) {
        RegistoCompetenciasTecnicas registoCompetencia = plataforma.getRegistoCompetenciaTecnica();
        List<CompetenciaTecnica> lista = registoCompetencia.getCompetenciaTecnicas();
        for (CompetenciaTecnica ct : lista) {
            if (ct.getCodigo().equalsIgnoreCase(cod)) {
                return ct;
            }
        }
        return null;
    }

//    public GrauProficiencia procurarEnum(String string) {
//        for (GrauProficiencia ct : GrauProficiencia.values()) {
//            if (ct.name().equalsIgnoreCase(string)) {
//                return ct;
//            }
//        }
//        return null;
//    }
}
