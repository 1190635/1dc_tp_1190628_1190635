/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.Candidatura;
import Model.Freelancer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class ListaCandidaturas {

    private List<Candidatura> candidaturas = new ArrayList<Candidatura>();

    public ListaCandidaturas() {
        candidaturas = new ArrayList<Candidatura>();
    }

    public List<Candidatura> getCandidaturas() {
        return candidaturas;
    }

    public Candidatura getCandidatura(String candID) {
        return null;
    }

    public boolean validaCandidatura(Candidatura cand) {
        return !candidaturas.contains(cand);
    }

    public Candidatura novaCandidatura(Freelancer freelancer, Date dataCand, double valor, int nrDias, String txtApresentacao, String txtMotivacao) {
        return new Candidatura(dataCand, valor, nrDias, txtApresentacao, txtMotivacao, freelancer);
    }

    public boolean addCandidatura(Candidatura candidatura) {
        return candidaturas.add(candidatura);
    }
}
