/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.CompetenciaTecnica;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class RegistoCompetenciasTecnicas {

    private List<CompetenciaTecnica> competenciasTecnicas;

    public RegistoCompetenciasTecnicas() {
        competenciasTecnicas = new ArrayList<CompetenciaTecnica>();
    }

    public List<CompetenciaTecnica> getCompetenciaTecnicas() {
        return competenciasTecnicas;
    }

    public void setCompetenciasTecnicas(List<CompetenciaTecnica> competenciasTecnicas) {
        this.competenciasTecnicas = competenciasTecnicas;
    }

    public boolean addCompetenciaTecnica(CompetenciaTecnica competenciaTecnica) {
        return competenciasTecnicas.add(competenciaTecnica);
    }

}
