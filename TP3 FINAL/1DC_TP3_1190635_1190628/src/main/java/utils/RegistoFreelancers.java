/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import Model.Freelancer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ptubb
 */
public class RegistoFreelancers {

    private List<Freelancer> freelancers;

    public RegistoFreelancers() {

        this.freelancers = new ArrayList<Freelancer>();

    }

    public List<Freelancer> getListaFreelancers() {
        return freelancers;
    }

    public void setFreelancers(List<Freelancer> freelancers) {
        this.freelancers = freelancers;
    }

    public boolean addFreelancer(Freelancer freelancer) {
        return freelancers.add(freelancer);
    }
}
