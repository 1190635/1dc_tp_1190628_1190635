package Model;

import UI.AlertaUI;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import javafx.scene.control.Alert;
import utils.RegistoCategorias;

public class Startup {

    public static List<Anuncio> readAnuncioFromFile(List<Tarefa> listaTarefas) throws FileNotFoundException {
        File file = new File(Constantes.ANUNCIO_PATH_NAME);
        Scanner in = new Scanner(file);

        List<Anuncio> listaAnuncios = new ArrayList<Anuncio>();
        int i = 0;
        while (in.hasNextLine() && i < listaTarefas.size()) {
            String linha = in.nextLine();
            String[] itens = linha.split("[/;]");
            int ano1 = Integer.parseInt(itens[0]);
            int mes1 = Integer.parseInt(itens[1]);
            int dia1 = Integer.parseInt(itens[2]);

            int ano2 = Integer.parseInt(itens[3]);
            int mes2 = Integer.parseInt(itens[4]);
            int dia2 = Integer.parseInt(itens[5]);

            int ano3 = Integer.parseInt(itens[6]);
            int mes3 = Integer.parseInt(itens[7]);
            int dia3 = Integer.parseInt(itens[8]);

            int ano4 = Integer.parseInt(itens[9]);
            int mes4 = Integer.parseInt(itens[10]);
            int dia4 = Integer.parseInt(itens[11]);

            int ano5 = Integer.parseInt(itens[12]);
            int mes5 = Integer.parseInt(itens[13]);
            int dia5 = Integer.parseInt(itens[14]);

            int ano6 = Integer.parseInt(itens[15]);
            int mes6 = Integer.parseInt(itens[16]);
            int dia6 = Integer.parseInt(itens[17]);

            Date date1 = new Date(ano1 - 1900, mes1 - 1, dia1);
            Date date2 = new Date(ano2 - 1900, mes2 - 1, dia2);
            Date date3 = new Date(ano3 - 1900, mes3 - 1, dia3);
            Date date4 = new Date(ano4 - 1900, mes4 - 1, dia4);
            Date date5 = new Date(ano5 - 1900, mes5 - 1, dia5);
            Date date6 = new Date(ano6 - 1900, mes6 - 1, dia6);

            Tarefa tarefa = listaTarefas.get(i);
            listaAnuncios.add(new Anuncio(tarefa, date1, date2, date3, date4, date5, date6, itens[18]));
            i++;
        }
        in.close();
        return listaAnuncios;
    }

    public Date createDate(String data) {
        return new Date(data);
    }

    public void readCaraterFromFile() {

    }

    public void readCandidaturaFromFile(Anuncio anuncio) {

        File file = new File(Constantes.CANDIDATURA_PATH_NAME);
        try (Scanner in = new Scanner(file)) {

            while (in.hasNextLine()) {
                String linha = in.nextLine();
                String[] array;
                array = linha.split(";");

                //Candidatura candidatura = new Candidatura(createDate(array[0]), Double.parseDouble(array[1]), Integer.parseInt(array[2]), array[3], array[4]);
                //anuncio.addCandidatura(candidatura);
            }

        } catch (FileNotFoundException fnfe) {
            AlertaUI.criarAlerta(Alert.AlertType.ERROR, "Startup", "Erro.", "Ficheiro" + Constantes.CANDIDATURA_PATH_NAME + "não encontrado");
        }

    }

    public List<CategoriaTarefa> readCategoriaTarefaFromFile() {

        File file = new File(Constantes.CATEGORIA_PATH_NAME);
        ArrayList<CategoriaTarefa> categorias = new ArrayList<>();

        try (Scanner in = new Scanner(file)) {

            while (in.hasNextLine()) {
                String linha = in.nextLine();
                String[] array;
                array = linha.split(";");

                CategoriaTarefa categoria = new CategoriaTarefa(array[0], array[1]);
                categorias.add(categoria);
            }

        } catch (FileNotFoundException fnfe) {
            AlertaUI.criarAlerta(Alert.AlertType.ERROR, "Startup", "Erro.", "Ficheiro" + Constantes.TAREFA_PATH_NAME + "não encontrado");
        }

        return categorias;

    }

    public List<CompetenciaTecnica> readCompetenciaTecnicaFromFile() throws FileNotFoundException {
        File file = new File(Constantes.COMPETENCIA_PATH_NAME);;
        Scanner in = new Scanner(file);
        List<CompetenciaTecnica> listaCompetencias = new ArrayList<>();
        while (in.hasNextLine()) {
            String linha = in.nextLine();
            String[] itens = linha.split("[;]");
            listaCompetencias.add(new CompetenciaTecnica(itens[0], itens[1], itens[2]));
        }
        in.close();
        return listaCompetencias;

    }

    public List<Freelancer> readFreelancerFromFile() throws FileNotFoundException {
        File file = new File(Constantes.FREELANCERS_PATH_NAME);
        Scanner in = new Scanner(file);
        List<Freelancer> freelancers = new ArrayList<Freelancer>();
        while (in.hasNextLine()) {
            String linha = in.nextLine();
            String[] itens = linha.split(";");
            freelancers.add(new Freelancer(itens[0], itens[1], itens[2], itens[3]));
        }
        in.close();
        return freelancers;
    }

    public void readGrauProficienciaFromFile() {

    }

    public void readListaCandidaturasFromFile() {

    }

    public void readProcessoSeriacaoFromFile() {

    }

    public static List<Tarefa> readTarefasFromFile(Plataforma plataforma) throws FileNotFoundException {
        ArrayList<Tarefa> listaTarefas = new ArrayList<>();
        File file = new File(Constantes.TAREFA_PATH_NAME);

        try (Scanner in = new Scanner(file)) {

            while (in.hasNextLine()) {
                String linha = in.nextLine();
                String[] array;
                array = linha.split(";");

                Tarefa tarefa = new Tarefa(array[0], array[1], array[2], array[3], Double.parseDouble(array[4]), Double.parseDouble(array[5]));
                listaTarefas.add(tarefa);
            }

        } catch (FileNotFoundException fnfe) {
            AlertaUI.criarAlerta(Alert.AlertType.ERROR, "Startup", "Erro.", "Ficheiro" + Constantes.TAREFA_PATH_NAME + "não encontrado");
        }

        return listaTarefas;
    }

    // public static CategoriaTarefa getCategoriaById(Plataforma plataforma, String id){
    //   RegistoCategorias rc = plataforma.getRegistoCategoria();
//        List<CategoriaTarefa> lista = rc.getCategorias();
//        for (CategoriaTarefa ct : lista) {
//            if (ct.getCodigo().equals(id)) {
//                return ct;
//            }
//        }
//        return null;
    //}
}
