package Model;

public class CompetenciaTecnica {

    public static enum GrausProficiencia {
        /**
         * The Muito baixo.
         */
        MUITO_BAIXO {
            public String toString() {
                return "1";
            }
        },
        /**
         * The Baixo.
         */
        BAIXO {
            public String toString() {
                return "2";
            }
        },
        /**
         * The Medio.
         */
        MEDIO {
            public String toString() {
                return "3";
            }
        },
        /**
         * The Alto.
         */
        ALTO {
            public String toString() {
                return "4";
            }
        },
        /**
         * The Muito alto.
         */
        MUITO_ALTO {
            public String toString() {
                return "5";
            }
        }
    }

    /**
     * Representa o codigo
     */
    String codigo;

    /**
     * Representa a descricao breve
     */
    String descBreve;

    /**
     * Representa a descricao detalhada
     */
    String descDetalhada;

    /**
     * Representa o grau de proficiencia
     */
    GrauProficiencia grauProficiencia;

    /**
     * Construtor da classe CompetenciaTecnica com todos os parametros
     */
    public CompetenciaTecnica(String cod, String desc_breve, String descr_det) {
        if ((cod == null) || (desc_breve == null) || (descr_det == null)
                || (cod.isEmpty()) || (desc_breve.isEmpty()) || (descr_det.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.codigo = cod;
        this.descBreve = desc_breve;
        this.descDetalhada = descr_det;
    }

    /**
     * Metodo de acesso da variavel de instancia codigo
     *
     * @return
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Representacao textual da classe CompetenciaTecnica
     *
     * @return
     */
    @Override
    public String toString() {
        return "Codigo:" + codigo + "\n"
                + "Descrição breve: " + descBreve + "\n"
                + "Descrição detalhada: " + descDetalhada;
    }

    /**
     * Metodo que compara uma competencia tecnica com outra comepetenciaTecninca
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        CompetenciaTecnica competenciaTecnica = (CompetenciaTecnica) outroObjeto;
        return codigo.equalsIgnoreCase(competenciaTecnica.getCodigo());
    }
}
