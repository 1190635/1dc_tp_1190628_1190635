package Model;

import java.util.ArrayList;
import java.util.List;

public class Freelancer {

    /**
     * Representa o nome do freelancer
     */
    private String nome;

    /**
     * Representa o NIF do freelancer
     */
    private String NIF;

    /**
     * Representa o telefone do freelancer
     */
    private String telefone;

    /**
     * Representa o email do freelancer
     */
    private String email;

    /**
     * Representa a lista de reconhecimentos
     */
    private List<Reconhecimento> lstReconhecimentos = new ArrayList<Reconhecimento>();

    /**
     * Construtor da classe Freelacer com todos os parametros
     */
    public Freelancer(String nome, String NIF, String telefone, String email) {
        if ((nome == null) || (NIF == null) || (telefone == null) || (email == null)
                || (Integer.parseInt(NIF) < 100000000) || (Integer.parseInt(NIF) > 999999999)
                || (nome.isEmpty()) || (NIF.isEmpty()) || (telefone.isEmpty()) || (email.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo ou vazio.");
        }
        this.nome = nome;
        this.NIF = NIF;
        this.telefone = telefone;
        this.email = email;
    }

    public Freelancer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Metodo de acesso da variavel de instancia nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo de acesso da variavel de instancia NIF
     */
    public String getNIF() {
        return NIF;
    }

    /**
     * Metodo de acesso da lstReconhecimentos
     */
    public List<Reconhecimento> getReconhecimento() {
        return lstReconhecimentos;
    }

    @Override
    public String toString() {
        return "Nome: " + nome + '\n'
                + "NIF: " + NIF + '\n'
                + "Telefone: " + telefone + '\n'
                + "Email: " + email + '\n' + "\n";
    }

    /**
     * Metodo que adiciona reconhecimento a lstReconhecimento
     */
    public boolean addReconhecimento(Reconhecimento reconhecimento) {
        return lstReconhecimentos.add(reconhecimento);
    }
}
