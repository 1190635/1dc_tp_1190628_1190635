/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ptubb
 */
public class CaraterCT {

    /**
     * Representa a obrigatoriedade da competencia tecnica
     */
    private boolean obrigatoria;

    /**
     * Representa o grau de proficiencia minimo
     */
    private GrauProficiencia grauProficienciaMinimo;

    /**
     * Representa a competencia tecnica
     */
    private CompetenciaTecnica competenciaTecnica;

    /**
     * Construtor da classe CaraterCT com todos os parametros
     */
    public CaraterCT(boolean obrigatoria, GrauProficiencia grauProficienciaMinimo, CompetenciaTecnica competenciaTecnica) {
        this.obrigatoria = obrigatoria;
        this.grauProficienciaMinimo = grauProficienciaMinimo;
        this.competenciaTecnica = competenciaTecnica;
    }

    public CaraterCT(boolean obrigatoria) {
        setObrigatoria(obrigatoria);
    }

    /**
     * Metodo de acesso da variavel de instancia obrigatoria
     */
    public boolean getIsObrigatoria() {
        return obrigatoria;
    }

    /**
     * Metodo de acesso do objeto grauProficienciaMinimo
     */
    public GrauProficiencia getGrauProficienciaMinimo() {
        return grauProficienciaMinimo;
    }

    /**
     * Metodo de acesso do objeto competenciaTecnica
     */
    public CompetenciaTecnica getCompetenciaTecnica() {
        return competenciaTecnica;
    }

    /**
     * Metodo de modificacao da intancia obrigatoria
     */
    public void setObrigatoria(boolean obrigatoria) {
        this.obrigatoria = obrigatoria;
    }

    /**
     * Metodo de modificacao do objeto competenciaTecnica
     *
     * @param competenciaTecnica valor a atribuir a competenciaTecnica
     */
    public void setCompetenciaTecnica(CompetenciaTecnica competenciaTecnica) {
        this.competenciaTecnica = competenciaTecnica;
    }

    /**
     * Representacao textual da classe CaraterCT
     */
    @Override
    public String toString() {
        return "Obrigatoria: " + obrigatoria + "\n"
                + "Grau Minimo:" + grauProficienciaMinimo + "\n"
                + "Competencia Tecnica: \n" + competenciaTecnica + "\n";
    }
}
