/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author ptubb
 */
class Constantes {
    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Tarefa guardadas
     */
    public static final String TAREFA_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaTarefas.txt";

    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Anuncio guardadas
     */
    public static final String ANUNCIO_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaAnuncios.txt";

    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Candidatura guardadas
     */
    public static final String CANDIDATURA_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaCandidaturas.txt";
    
    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe CategoriaTarefa guardadas
     */
    public static final String CATEGORIA_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaCategorias.txt";
    
     /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Freelancer guardadas
     */
    public static final String FREELANCERS_PATH_NAME = "src\\main\\resources\\ClassFiles\\freelancers.txt";
    
    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Reconhecimento guardadas
     */
    public static final String RECONHECIMENTO_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaReconhecimentos.txt";
    
    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe CaraterCT guardadas
     */
    public static final String CARATER_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaCaracter.txt";
    
    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe CaraterCT guardadas
     */
    public static final String COMPETENCIA_PATH_NAME = "src\\main\\resources\\ClassFiles\\ListaCaracter.txt";

}
