package Model;

import java.util.Date;
import utils.ListaCandidaturas;

public class Anuncio {

    /**
     * Representa a data de inicio da publicitacao
     */
    private Date dtInicioPublicitacao;

    /**
     * Representa a data final da publicitacao
     */
    private Date dtFimPublicitacao;

    /**
     * Representa a data de inicio da candidatura
     */
    private Date dtInicioCandidatura;

    /**
     * Representa a data final da Candidatura
     */
    private Date dtFimCandidatura;

    /**
     * Representa a data de inicio da seriacao
     */
    private Date dtInicioSeriacao;

    /**
     * Representa a data final da seriacao
     */
    private Date dtFimSeriacao;

    /**
     * Representa o tipo de regimento aplicado ao anuncio
     */
    private TipoRegimento tipoRegimento;

    /**
     * Representa a lista de candidaturas a um anuncio
     */
    private static ListaCandidaturas listaCandidaturas;

    /**
     * Representa o processo de seriacao espoletado por este anuncio
     */
    private static ProcessoSeriacao processoSeriacao;

    /**
     * Representa a tarefa
     */
    private Tarefa tarefa;

    /**
     * Representa o id do anuncio
     */
    private String anuncioID;

    /**
     * Construtor da classe Anuncio com todos os parametros
     */
    public Anuncio(Tarefa tarefa, Date dtInicioPublicitacao, Date dtFimPublicitacao, Date dtInicioCandidatura, Date dtFimCandidatura, Date dtInicioSeriacao, Date dtFimSeriacao, String anuncioID) {
        if ((tarefa == null) || (dtInicioPublicitacao == null) || (dtFimPublicitacao == null) || (dtInicioCandidatura == null) || (dtFimCandidatura == null)
                || (dtInicioSeriacao == null) || (dtFimSeriacao == null) || (anuncioID == null) || (anuncioID.trim().isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo.");
        }
        this.tarefa = tarefa;
        this.dtInicioPublicitacao = dtInicioPublicitacao;
        this.dtFimPublicitacao = dtFimPublicitacao;
        this.dtInicioCandidatura = dtInicioCandidatura;
        this.dtFimCandidatura = dtFimCandidatura;
        this.dtInicioSeriacao = dtInicioSeriacao;
        this.dtFimSeriacao = dtFimSeriacao;
        this.tipoRegimento = tipoRegimento;
        this.anuncioID = anuncioID;
        this.listaCandidaturas = new ListaCandidaturas();
    }

    /**
     * Metodo de acesso ao objeto listaCandidaturas
     */
    public ListaCandidaturas getListaCandidaturas() {
        return listaCandidaturas;
    }

    /**
     * Metodo de acesso ao objeto getTipoRegimento
     */
    public TipoRegimento getTipoRegimento() {
        return tipoRegimento;
    }

    /**
     * Metodo de acesso ao objeto Tarefa
     */
    public Tarefa getTarefa() {
        return tarefa;
    }

    /**
     * Metodo de acesso da variavel de instancia anuncioID
     */
    public String getAnuncioID() {
        return anuncioID;
    }

    /**
     * Metodo de modificacao do objeto processoSeriacao
     *
     * @param processoSeriacao
     */
    public static void setProcessoSeriacao(ProcessoSeriacao processoSeriacao) {
        Anuncio.processoSeriacao = processoSeriacao;
    }

    /**
     * Representacao textual da classe Anuncio
     *
     * @return
     */
    @Override
    public String toString() {
        return String.format("Tarefa: %s\nInicio da publicitação: %s\nFim da publicitação: %s\n\nInicio das candidaturas: %s\nFim das canddaturas: %s\n\nInicio da Seriação: %s\nFim da Seriação: %s\n", this.tarefa, this.dtInicioPublicitacao, this.dtFimPublicitacao, this.dtInicioCandidatura, this.dtFimCandidatura, this.dtInicioSeriacao, this.dtFimSeriacao);
    }

    /**
     * Metodo que inicia uma novo processo de seriacao
     */
    public ProcessoSeriacao novoProcessoSeriacao() {
        return new ProcessoSeriacao(this.getTipoRegimento());
    }

    /**
     * Metodo que regista o processo de seriacao
     */
    public void registaProcessoSeriacao(ProcessoSeriacao ps) {
        setProcessoSeriacao(ps);
    }

    /**
     * Metodo que adiciona candidatura a lista de candidaturas
     */
    public void addCandidatura(Candidatura cand) {
        listaCandidaturas.addCandidatura(cand);
    }
}
