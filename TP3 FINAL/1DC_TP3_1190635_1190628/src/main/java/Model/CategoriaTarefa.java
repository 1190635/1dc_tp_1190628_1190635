package Model;

import java.util.List;

public class CategoriaTarefa {

    /**
     * Representa o codigo
     */
    private String codigo;

    /**
     * Representa a descricao
     */
    private String des;

    /**
     * Representa o id da categoria de tarefa
     */
    private String id;

    /**
     * Representa a lista de caraterCT
     */
    private List<CaraterCT> lstCaraterCT;

    /**
     * Construtor da classe CategoriaTarefa com todos os parametros
     */
    public CategoriaTarefa(String codigo, String des) {
        if ((codigo == null) || (des == null) || (des.isEmpty()) || (codigo.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.codigo = codigo;
        this.des = des;
    }

    /**
     * Metodo de acesso a listCaraterCT
     */
    public List<CaraterCT> getListaCaraterCT() {
        return lstCaraterCT;
    }

    /**
     * Metodo de aceso da variavel de instancia codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo de acesso da vairavel de instancia id
     *
     * @return
     */
    public String getId() {
        return id;
    }

    /**
     * Reprentacao textual da classse CategoriaTarefa
     *
     * @return
     */
    @Override
    public String toString() {
        return "Categoria: " + codigo + '\n'
                + "Descricao: " + des + '\n';
    }

    /**
     * Metodo que adiciona caraterCT a lstCaraterCT
     */
    public boolean addCaraterCt(CaraterCT caraterCT) {
        return lstCaraterCT.add(caraterCT);
    }

}
