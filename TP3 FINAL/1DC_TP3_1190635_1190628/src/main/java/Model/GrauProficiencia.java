package Model;

/**
 * The enum Graus proficiencia.
 */
public enum GrauProficiencia {
    /**
     * Representa o grau muito baixo.
     */
    MUITO_BAIXO {
        public String toString() {
            return "1";
        }
    },
    /**
     * Representa o grau baixo.
     */
    BAIXO {
        public String toString() {
            return "2";
        }
    },
    /**
     * Representa o grau ,edio.
     */
    MEDIO {
        public String toString() {
            return "3";
        }
    },
    /**
     * Representa o grau alto.
     */
    ALTO {
        public String toString() {
            return "4";
        }
    },
    /**
     * Representa o grau muito alto.
     */
    MUITO_ALTO {
        public String toString() {
            return "5";
        }
    }

}
