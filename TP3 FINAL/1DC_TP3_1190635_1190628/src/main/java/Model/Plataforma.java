package Model;

import utils.ListaTarefas;
import utils.RegistoAnuncios;
import utils.RegistoCategorias;
import utils.RegistoCompetenciasTecnicas;
import utils.RegistoFreelancers;
import utils.RegistoTiposRegimento;

public class Plataforma {

    /**
     * Representa a designacao da platafor,a
     */
    private String designacao;

    /**
     * Representa a lista de tarefas
     */
    private ListaTarefas lstTarefas;

    /**
     * Representa os tipos de regimento
     */
    private RegistoTiposRegimento registoTipoRegimento;

    /**
     * Representa os anuncios
     */
    private RegistoAnuncios registoAnuncios;

    /**
     * Representa o freelancer
     */
    private RegistoFreelancers registoFreelancer;

    /**
     * Representa a categoria
     */
    private RegistoCategorias registoCategoria;

    /**
     * Representa a competencia tecnica
     */
    private RegistoCompetenciasTecnicas registoCompetenciaTecnica;

    /**
     * Construtor da classe Plataforma com todos os parametros
     *
     * @param designacao
     */
    public Plataforma(String designacao) {
        this.designacao = designacao;
        registoTipoRegimento = new RegistoTiposRegimento();
        registoAnuncios = new RegistoAnuncios();
        registoFreelancer = new RegistoFreelancers();
        registoCategoria = new RegistoCategorias();
        registoCompetenciaTecnica = new RegistoCompetenciasTecnicas();
    }

    /**
     * Metodo de acesso a lstTarefas
     */
    public ListaTarefas getListaTarefas() {
        return lstTarefas;
    }

    /**
     * Metodo de acesso a anuncios
     */
    public RegistoAnuncios getRegistoAnuncios() {
        return registoAnuncios;
    }

    /**
     * Metodo de acesso ao tipo de regimento
     */
    public RegistoTiposRegimento getRegistoTipoRegimento() {
        return registoTipoRegimento;
    }

    /**
     * Metodo de acesso ao freelancer
     */
    public RegistoFreelancers getRegistoFreelancer() {
        return registoFreelancer;
    }

    /**
     * Metodo de acesso a categoria
     */
    public RegistoCategorias getRegistoCategoria() {
        return registoCategoria;
    }

    /**
     * Metodo de acesso as competencias tecnicas
     */
    public RegistoCompetenciasTecnicas getRegistoCompetenciaTecnica() {
        return registoCompetenciaTecnica;
    }

    /**
     * Metodo de modificacao da lstTarefas
     */
    public void setListaTarefas(ListaTarefas lstTarefas) {
        this.lstTarefas = lstTarefas;
    }

    /**
     * Metodo de modificacao da variavel de instancia
     */
    public void setDesignacao(String designacao) {
        if (designacao == null) {
            throw new IllegalArgumentException("Designação da plataforma inválida");
        }
        this.designacao = designacao;
    }

    /**
     * Metodo de modificacao da categoria
     */
    public void setRegistoCategoria(RegistoCategorias registoCategoria) {
        this.registoCategoria = registoCategoria;
    }
}
