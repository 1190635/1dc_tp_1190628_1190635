/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;

import Model.Candidatura;
import Model.Tarefa;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressIndicator;

/**
 *
 * @author Gonçalo
 */
public class ProcessoSeriacaoUI implements Initializable {

    private SelecionarAnuncioUI selecionarTarefaUI;

    @FXML
    private ListView<Candidatura> lstViewCandidaturas;
    @FXML
    private Label lblSeriarAnuncioPergunta;
    @FXML
    private Button btnConfirmarInicioSeriacao;
    @FXML
    private Button btnCancelarSeriacao;

    private Scene anuncioEmSeriacaoScene;
    @FXML
    private ComboBox<?> cmbBoxTiposRegimento;

    public void associarParentUI(SelecionarAnuncioUI selecionarTarefaUI) {
        this.selecionarTarefaUI = selecionarTarefaUI;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AnuncioEmSeriacaoUI.fxml"));
            Parent root = loader.load();

            anuncioEmSeriacaoScene = new Scene(root);

            // atualizaListViewCandidaturas();
        } catch (IOException ex) {
            AlertaUI.criarAlerta(Alert.AlertType.ERROR, MainApp.TITULO_APLICACAO, "Erro.", ex.getMessage());
        }
    }

    private void atualizaListViewCandidaturas() {
        lstViewCandidaturas.getItems().setAll(selecionarTarefaUI.getAppController().getCandidaturas());
    }

    @FXML
    private void ConfirmarInicioSeriacaoAction(ActionEvent event) {
    }

    @FXML
    private void cancelarSeriacaoAction(ActionEvent event) {
        encerrarProcessoSeriacaoUI(event);
    }

    private void encerrarProcessoSeriacaoUI(ActionEvent event) {
        this.lstViewCandidaturas.getItems().setAll();

        ((Node) event.getSource()).getScene().getWindow().hide();
    }

}
