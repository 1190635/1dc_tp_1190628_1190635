package UI;

import Controller.SeriarAnuncioController;
import Model.Anuncio;
import Model.Tarefa;
import static UI.MainApp.TITULO_APLICACAO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class SelecionarAnuncioUI implements Initializable {

    private Stage processoSeriacaoStage;
    private SeriarAnuncioController appController;

    private Label label;
    @FXML
    private Label lblSelecionarTarefa;
    @FXML
    private Button btnConfirmarSelecaoTarefa;
    @FXML
    private Button btnCancelarSelecaoTarefa;
    @FXML
    private ListView<Anuncio> lstViewAnuncio;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ProcessoSeriacaoUI.fxml"));
            Parent root = loader.load();

            Scene scene = new Scene(root);

            processoSeriacaoStage = new Stage();
            processoSeriacaoStage.initModality(Modality.APPLICATION_MODAL);
            processoSeriacaoStage.setTitle("Seriar Anúncio");
            processoSeriacaoStage.setResizable(false);
            processoSeriacaoStage.setScene(scene);

            appController = new SeriarAnuncioController();
            atualizarListaAnuncios();

            ProcessoSeriacaoUI processoSeriacaoUI = loader.getController();
            processoSeriacaoUI.associarParentUI(this);
        } catch (IOException ex) {
            AlertaUI.criarAlerta(Alert.AlertType.ERROR, MainApp.TITULO_APLICACAO, "Erro.", ex.getMessage());
        }
    }

    public void atualizarListaAnuncios() {
        lstViewAnuncio.getItems().setAll(appController.getAnuncios());
    }

    public SeriarAnuncioController getAppController() {
        return appController;
    }

    @FXML
    private void confirmarTarefaAction(ActionEvent event) {
        processoSeriacaoStage.show();
    }

    @FXML
    private void checkSelectionAction(MouseEvent event) {
        if (lstViewAnuncio.getSelectionModel().getSelectedItem() != null) {
            btnConfirmarSelecaoTarefa.setDisable(false);
        }
    }

    @FXML
    private void btnCancelarSelecaoTarefaAction(ActionEvent event) {
        Stage stage = (Stage) btnCancelarSelecaoTarefa.getScene().getWindow();

        Alert alerta = AlertaUI.criarAlerta(Alert.AlertType.CONFIRMATION, TITULO_APLICACAO,
                "Confirmação da ação.", "Deseja mesmo cancelar a seriação do anúncio?");

        if (alerta.showAndWait().get() == ButtonType.CANCEL) {
            event.consume();
        } else {
            stage.close();
        }
    }

}
