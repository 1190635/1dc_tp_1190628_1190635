package pt.ipp.isep.dei;

public class Automovel implements Alugavel, Vendavel {

    /**
     * Representa a marca do automovel
     */
    private String marca;

    /**
     * Representa o modelo do automovel
     */
    private String modelo;

    /**
     * Valor inserido pelo anunciante para aluguer
     */
    private double valorAluguerInicial;

    /**
     * Valor inserido pelo anunciante para venda
     */
    private double valorVendaInicial;

    /**
     * Valor a ser usado na omissao da variavel marca
     */
    private static final String MARCA_OMISSAO="Sem marca";

    /**
     * Valor a ser usado na omissao da variavel modelo
     */
    private static final String MODELO_OMISSAO="Sem modelo";

    /**
     * Valor a ser usado na omissao da variavel valorAluguerInicial
     */
    private static final double VALOR_ALUGUER_INICIAL_OMISSAO =0;

    /**
     * Valor a ser usado na omissao da variavel valorVendaInicial
     */
    private static final double VALOR_VENDA_INICIAL_OMISSAO =0;

    /**
     * Construtor da classe Automovel com todos os parametros
     */
    public Automovel(String marca, String modelo, double valorAluguerInicial, double valorVendaInicial){
     this.marca=marca;
     this.modelo=modelo;
     this.valorAluguerInicial = valorAluguerInicial;
     this.valorVendaInicial = valorVendaInicial;
    }

    /**
     * Contrutor da classe Automovel sem parametros
     */
    public Automovel(){
        marca=MARCA_OMISSAO;
        modelo=MODELO_OMISSAO;
        valorAluguerInicial = VALOR_ALUGUER_INICIAL_OMISSAO;
        valorVendaInicial = VALOR_VENDA_INICIAL_OMISSAO;
    }

    /**
     * Metodo de acesso da variavel de instancia marca
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo de acesso da variavel de instancia modelo
     */
    public String getModelo() {
        return modelo;
    }


     /**
     * Metodo de acesso da variavel de instancia valorAluguerInicial
     */
    public double getValorAluguerInicial(){

        return valorAluguerInicial;
    }

    /**
     * Metodo de acesso da variavel de instancia valorVendaInicial
     */
    public double getValorVendaInicial(){

        return valorVendaInicial;
    }

    /**
     * Metodo de modificacao da variavel de instancia marca
     * @param marca valor a atribuir a varivavel marca
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }
 
    /**
     * Metodo de modificacao da variavel de instancia modelo
     * @param modelo valor a atribuir a varivavel modelo
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
     
    /**
     * Metodo de modificacao da variavel de instancia valorAluguerInicial
     * @param valorAluguerInicial valor a atribuir a varivavel valorAluguerPretendido
     */
    public void setValorAluguerInicial(double valorAluguerInicial){
     this.valorAluguerInicial = valorAluguerInicial;
    }
 
    /**
     * Metodo de modificacao da variavel de instancia valorVendaInicial
     * @param valorVendaInicial valor a atribuir a varivavel valorVendaInicial
     */
    public void setValorVendaInicial(double valorVendaInicial){

        this.valorVendaInicial = valorVendaInicial;
    }

    /**
     * Representacao textual da classe Automovel
     * @return string representativa da classe Automovel
     */
    @Override
    public String toString() {
        return "Automovel {" +
                "Marca='" + marca + '\'' +
                ", Modelo='" + modelo + '\'' +
                ", Valor de Aluguer Inserido= " + getValorAluguerInicial() +
                ", Valor de Venda Inserido= " + getValorVendaInicial() +
                ", Valor de Aluguer=" + valorAluguerFinal() +
                ", Valor de Venda=" + valorVendaFinal() +
                '}';
    }

    /**
     * Calcula o valor da venda depois de aplicadas as taxas da plataforma olxyz
     * @return valor da venda final
     */
    public double valorVendaFinal(){

        return valorVendaInicial *(1+Vendavel.taxaVenda);
    }

    /**
     * Calcula o valor do alugavel depois de aplicadas as taxas da plataforma olxyz
     * @return valor do alugavel final
     */
    public double valorAluguerFinal(){
        return valorAluguerInicial *(1+Alugavel.taxaAluguer);
    }
}
