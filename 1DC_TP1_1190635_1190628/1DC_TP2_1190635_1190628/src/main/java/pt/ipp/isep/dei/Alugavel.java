package pt.ipp.isep.dei;

public interface Alugavel {

    /**
     * Taxa de aluguer aplicada sobre o valor pretendido de aluguer de um anuncio
     */
    double taxaAluguer = 0.05;

    /**
     * Calcula o valor do alugavel depois de aplicadas as taxas da plataforma olxyz
     * @return valor do alugavel final
     */
    double valorAluguerFinal();
}
