package pt.ipp.isep.dei;

import java.util.ArrayList;

public class Anunciante {

    /**
     * Representa o nome do anunciante
     */
    private String nome;

    /**
     * Representa o endereco do anunciante
     */
    private Endereco endereco;

    /**
     * Lista pertencente a um anunciante que contem os anuncios de venda
     */
    ArrayList<Vendavel> vendaveis;

    /**
     * Lista pertencente a um anunciante que contem os anuncios de aluguer
     */
    ArrayList<Alugavel> alugaveis;

    /**
     * Valor a ser usado na omissao da variavel nome
     */
    private static final String NOME_OMISSAO="Sem nome";

    /**
     * Construtor com todos os parametros
     */
    public Anunciante(String nome, Endereco endereco) {
        this.nome = nome;
        this.endereco = new Endereco(endereco);
        this.vendaveis = new ArrayList<Vendavel>();
        this.alugaveis = new ArrayList<Alugavel>();
    }

    /**
     * Construtor sem parametros
     */
    public Anunciante() {
        this.nome = NOME_OMISSAO;
        this.endereco = new Endereco();
        this.vendaveis = new ArrayList<Vendavel>();
        this.alugaveis = new ArrayList<Alugavel>();
    }

    /**
     * Metodo de acesso da variavel de instancia nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo de acesso do objeto endereco
     */
    public Endereco getEndereco() {
        return endereco;
    }

    /**
     * Metodo de modificacao da variavel de instancia nome
     *
     * @param nome novo valor para a variavel nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Representação textual da classe Anunciante
     *
     * @return string representativa da classe Anunciante
     */
    @Override
    public String toString() {
        return "Anunciante " +
                "Nome= '" + nome + '\'' +
                "\n" + endereco.toString() +
                "\nLista de vendaveis=" + vendaveis +
                "\nLista de alugaveis=" + alugaveis;
    }

    /**
     * Adiciona um alugavel à lista de alugaveis com limite  de até 3 alugaveis
     *
     * @param alugavel objeto que implementa a interface alugavel que ira ser adicionado à lista
     */
    public void addAlugavel(Alugavel alugavel) {
        if (alugaveis.size() < 3) {
            this.alugaveis.add(alugavel);
        } else {
            System.out.println("Já possui 3 anuncios de Alugaveis!");
        }
    }

    /**
     * Adiciona um vendavel à lista de vendaveis com limite  de até 2 vendaveis
     *
     * @param vendavel objeto que implementa a interface vendavel que ira ser adicionado à lista
     */
    public void addVendavel(Vendavel vendavel) {
        if (vendaveis.size() < 2) {              //Testar size
            this.vendaveis.add(vendavel);

        } else {
            System.out.println("Já possui 2 anuncios de Vendaveis!");
        }
    }

    /**
     * Retorna o objeto alugavel mais caro do anunciante
     *
     * @return objeto alugavel mais caro do anunciante
     */
    public Alugavel getAlugavelMaisCaro() {
        Alugavel aMaisCaro = alugaveis.get(0);
        double maisCaro = 0;
        for (Alugavel alugavel : alugaveis) {
            if (alugavel.valorAluguerFinal() > maisCaro) {
                aMaisCaro = alugavel;
                maisCaro = alugavel.valorAluguerFinal();
            }
        }
        return aMaisCaro;
    }

    /**
     * Retorna o total possivel das suas vendas
     *
     * @return total de vendas
     */
    public double getTotalVendas() {
        double total = 0;
        for (Vendavel vendavel : vendaveis) {
            total += vendavel.valorVendaFinal();
        }

        return total;
    }

    /**
     * Retorna a quantidade de alugaveis do anunciante
     *
     * @return quantidade de alugaveis
     */
    public int getQtdAlugaveis() {
        return (alugaveis.size());
    }

    /**
     * Retorna a quantidade de alugaveis do anunciante
     *
     * @return quantidade de alugaveis
     */
    public int getQtdVendaveis() {
        return (vendaveis.size());
    }
}
