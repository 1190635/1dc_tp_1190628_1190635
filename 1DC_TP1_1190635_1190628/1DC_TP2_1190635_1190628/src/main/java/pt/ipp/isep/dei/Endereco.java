package pt.ipp.isep.dei;


public class Endereco {

    /**
     * Representa a rua/morada
     */
    private String rua;

    /**
     * Representa o Codigo Postal
     */
    private String codigoPostal;

    /**
     * Representa a localidade (Ex.: Maia)
     */
    private String localidade;

    /**
     * Valor a ser usado na omissao da variavel rua
     */
    private static final String RUA_OMISSAO="Sem rua";

    /**
     * Valor a ser usado na omissao da variavel codigoPostal
     */
    private static final String CODIGO_POSTAL_OMISSAO="Sem código postal";

    /**
     * Valor a ser usado na omissao da variavel localidade
     */
    private static final String LOCALIDADE_OMISSAO="Sem localidade";

    /**
     * Construtor da classe Endereco com todos os parametros
     */
    public Endereco(String rua,String codigoPostal,String localidade){
     this.rua=rua;
     this.codigoPostal=codigoPostal;
     this.localidade=localidade;
    }

    /**
     * Contrutor da classe Endereco sem parametros
     */
   public Endereco(){
       this.rua=RUA_OMISSAO;
       this.codigoPostal=CODIGO_POSTAL_OMISSAO;
       this.localidade=LOCALIDADE_OMISSAO;
   }

    /**
     * Construtor de copia da classe Endereco
     */
   public Endereco(Endereco outroEndereco){
       this.rua=outroEndereco.rua;
       this.codigoPostal=outroEndereco.codigoPostal;
       this.localidade=outroEndereco.localidade;
   }

    /**
     * Metodo de acesso da variavel de instancia rua
     */
    public String getRua() {
        return rua;
    }

    /**
     * Metodo de acesso da variavel de instancia codigoPostal
     */
    public String getCodigoPostal() {
        return codigoPostal;
    }

    /**
     * Metodo de acesso da variavel de instancia localidade
     */
    public String getLocalidade() {

        return localidade;
    }

    /**
     * Metodo de modificacao do Endereco
     * @param rua valor a atribuir a variavel rua
     * @param codigoPostal valor a atribuir a variavel codigoPostal
     * @param localidade valor a atribuir a variavel localidade
     */
    public void setEndereco(String rua, String codigoPostal,String localidade){
      this.rua=rua;
      this.codigoPostal=codigoPostal;
      this.localidade=localidade;
}
    /**
     * Metodo de modificacao da variavel de instancia rua
     * @param rua valor a atribuir a varivavel rua
     */
    public void setRua(String rua) {
        this.rua = rua;
    }

    /**
     * Metodo de modificacao da variavel de instancia codigoPostal
     * @param codigoPostal valor a atribuir a varivavel codigoPostal
     */
    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    /**
     * Metodo de modificacao da variavel de instancia localidade
     * @param localidade valor a atribuir a varivavel localidade
     */
    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    /**
     * Representacao textual da classe Endereco
     * @return string representativa da classe Endereco
     */
    @Override
    public String toString() {
        return  "Endereco= "+rua +
                ", Codigo Postal= " + codigoPostal +
                ", Localidade= " + localidade;
    }
}
