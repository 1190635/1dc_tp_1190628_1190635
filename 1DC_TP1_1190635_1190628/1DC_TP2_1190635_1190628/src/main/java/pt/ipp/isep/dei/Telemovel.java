package pt.ipp.isep.dei;

public class Telemovel implements Vendavel{

    /**
     * Designacao do telemovel
     */
    private String designacao;

    /**
     * Representa o valor de venda inserido pelo utilizador
     */
    private double valorVendaInicial;

    /**
     * String a ser usada na omissao da variavel designacao
     */
    private static final String DESIGNACAO_OMISSAO="sem designação";

    /**
     * Valor a ser usado na omissao da variavel valorVendaInicial
     */
    private static final double VALOR_VENDA_INICIAL_OMISSAO =0;

    /**
     * Construtor com todos os parametros
     */
    public Telemovel(String designacao,double valorVendaPretendido){
        this.designacao=designacao;
        this.valorVendaInicial =valorVendaPretendido;
    }

    /**
     * Construtor sem parametros
     */
    public Telemovel(){
        this.designacao=DESIGNACAO_OMISSAO;
        this.valorVendaInicial = VALOR_VENDA_INICIAL_OMISSAO;

    }

    /**
     * Metodo de acesso da variavel de instancia designacao
     * @return variavel designacao
     */
    public String getDesignacao() {
        return designacao;
    }

    /**
     * Metodo de acesso da variavel de instancia valorVendaInicial
     * @return variavel valorVendaInicial
     */
    public double getValorVendaInicial(){
        return valorVendaInicial;
    }

    /**
     * Metodo de modificacao da variavel de instancia designacao
     * @param designacao novo valor para a variavel designacao
     */
    public void setDesignacao(String designacao) {
        this.designacao = designacao;
    }

    /**
     * Metodo de modificacao da variavel de instancia valorVendaInicial
     * @param valorVendaInicial novo valor para a variavel valorVendaInicial
     */
    public void setValorVendaInicial(double valorVendaInicial){
        this.valorVendaInicial = valorVendaInicial;
    }

    /**
     * Representacao textual da classe Telemovel
     * @return string representativa da classe telemovel
     */
    @Override
    public String toString() {
        return "Telemovel {" +
                "Designacao= '" + designacao + '\'' +
                ", Valor de Venda Pretendido=" + valorVendaInicial +
                ", Valor de Venda=" + valorVendaFinal() +
                '}';
    }

    /**
     * Calcula o valor da venda depois de aplicadas as taxas da plataforma olxyz
     * @return valor da venda final
     */
    public double valorVendaFinal(){
        return valorVendaInicial *(1+Vendavel.taxaVenda);
    }
}
