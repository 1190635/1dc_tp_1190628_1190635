package pt.ipp.isep.dei;

public interface Vendavel {

    /**
     * Taxa de venda aplicada sobre o valor pretendido de venda de um anuncio
     */
    double taxaVenda = 0.03;

    /**
     * Calcula o valor da venda depois de aplicadas as taxas da plataforma olxyz
     * @return valor da venda final
     */
    public double valorVendaFinal();
}
