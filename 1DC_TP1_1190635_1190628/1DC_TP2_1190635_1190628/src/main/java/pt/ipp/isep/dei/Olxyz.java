package pt.ipp.isep.dei;

public class  Olxyz {

    public static void main(String[] args) {
        int qtdAnunciantes=0; int totalAlugaveis=0; int totalVendaveis=0;int c=0;

        //b)1.
        Anunciante[] array = new Anunciante[5];

        //b)2.
        Endereco e1 = new Endereco("Rua central de vilar","4585-720","Sobreira");
        Telemovel t1 = new Telemovel("RealMe 6",230);
        Apartamento apart1 = new Apartamento(50,e1,1000);
        Automovel auto1 = new Automovel("tesla","model 3",13200,35000);
        Anunciante a1= new Anunciante("Goncalo Carvalho",e1);
        a1.addAlugavel(auto1); a1.addAlugavel(apart1);
        a1.addVendavel(t1);
        array[0]=a1;

        //b)3.
        Endereco e2 = new Endereco("Rua jose regio", "4500-120" , "Maia");
        Automovel auto2 = new Automovel("tesla","model x",10000,100100);
        Apartamento apart2 = new Apartamento(75,e2,1500);
        Telemovel t2 = new Telemovel("Samsung s20",999.99);
        Anunciante a2 = new Anunciante("Goncalo Ferreira",e2);
        a2.addVendavel(auto2); a2.addVendavel(t2);
        a2.addAlugavel(apart2);
        array[1]=a2;

        //b)4.
        Endereco e3 = new Endereco("Avenida da Liberdade","4302-627","Porto");
        Endereco e31 = new Endereco("Avenida dos combatentes","2500-720","Porto");
        Apartamento apart31 = new Apartamento(20,e31,25000);
        Apartamento apart32 = new Apartamento(40,e3,30000);
        Anunciante a3 = new Anunciante("Chico Tina",e3);
        a3.addAlugavel(apart31); a3.addAlugavel(apart32);
        array[2]=a3;

        for(int i=0;i<array.length;i++){
            if(array[i]!=null){
                qtdAnunciantes++;
            }
        }
        
        //b)5.
        System.out.print("Quantidade de artigos disponiveis para aluguer na plataforma Olxyz = ");

        for(int i=0 ; i<qtdAnunciantes;i++){
            totalAlugaveis+= array[i].getQtdAlugaveis();
        }

        System.out.println(totalAlugaveis);

        //b)6.
         System.out.printf("\n|          Nome          |                                              Endereco                                              | Total Possivel De Vendas |\n"
                            + "|========================|====================================================================================================|==========================|\n");
        for(int i =0;i<qtdAnunciantes;i++){

            if (array[i].getQtdVendaveis()>0) {
                System.out.printf("|%-24s|%-100s|%26.2f|\n",
                        array[i].getNome(),array[i].getEndereco(),array[i].getTotalVendas());
            }

        }

        //b)7.
        System.out.printf("\n|          Nome          |                                                                                 Alugavel                                                                                 | Preco de aluguer |\n"
                + "|========================|==========================================================================================================================================================================|==================|\n");
        for(int i =0;i<qtdAnunciantes;i++){

            if (array[i].getQtdAlugaveis()>0) {
                System.out.printf("|%-24s|%-170s|%18.2f|\n",
                        array[i].getNome(),array[i].getAlugavelMaisCaro().toString(),array[i].getAlugavelMaisCaro().valorAluguerFinal());
            }

        }
    }

}
