package pt.ipp.isep.dei;

public class Apartamento implements Alugavel{

    /**
     * objeto endereco da classe Endereco
     */
    private Endereco endereco;

    /**
     * Representa a area do apartamento
     */
    private double area;

    /**
     * Representa o valor de aluguer inserido pelo anunciante
     */
    private double valorAluguerInicial;

    /**
     * Valor a ser usado na omissao da variavel area
     */
    private static final double AREA_OMISSAO=0;

    /**
     * Valor a ser usado na omissao da variavel valorAluguerInicial
     */
    private static final double VALOR_ALUGUER_INICIAL_OMISSAO =0;

    /**
     * Construtor da classe Apartamento com todos os parametros
     */
    public Apartamento(double area, Endereco endereco,double valorAluguerInicial){
        this.area=area;
        this.endereco= new Endereco(endereco);
        this.valorAluguerInicial = valorAluguerInicial;
    }

    /**
     * Contrutor da classe Apartamento sem parametros
     */
    public Apartamento(){
        endereco=new Endereco();
        area=AREA_OMISSAO;
        valorAluguerInicial = VALOR_ALUGUER_INICIAL_OMISSAO;
    }

    /**
     * Metodo de acesso do objeto endereco da instancia da classe Apartamento
     */
    public Endereco getEndereco() {

        return new Endereco(endereco);
    }

    /**
     * Metodo de acesso da variavel de instancia area
     * @return variavel area
     */
    public double getArea() {

        return area;
    }

    /**
     * Metodo de acesso da variavel de instancia valorAluguerIncial
     */
    public double getValorAluguerInicial(){
        return valorAluguerInicial;
    }

    /**
     * Metodo de modificacao do objeto endereco da instancia da classe Apartamento
     * @param endereco valor a atribuir a varivavel endereco
     */
    public void setEndereco(Endereco endereco) {
        this.endereco.setEndereco(endereco.getRua(),endereco.getCodigoPostal(),endereco.getLocalidade());
    }

    /**
     * Metodo de modificacao da variavel de instancia area
     * @param area valor a atribuir a varivavel area
     */
    public void setArea(double area) {
        this.area = area;
    }

    /**
     * Metodo de modificacao da variavel de instancia valorAluguerInicial
     * @param valorAluguerInicial valor a atribuir a varivavel valorAluguerInicial
     */
    public void setValorAluguerInicial(double valorAluguerInicial){
      this.valorAluguerInicial = valorAluguerInicial;
    }

    /**
     * Representação textual da classe Apartamento
     * @return string representativa da classe Apartamento
     */
    @Override
    public String toString() {
        return "Apartamento {" +
                endereco +
                ", Area=" + area +
                ", Valor de Aluguer Inserido=" + valorAluguerInicial +
                ", Valor de Aluguer=" + valorAluguerFinal() +
                '}';
    }

    /**
     * Calcula o valor do alugavel depois de aplicadas as taxas da plataforma olxyz
     * @return valor do alugavel final
     */
    public double valorAluguerFinal(){
        return valorAluguerInicial *(1+Alugavel.taxaAluguer);
    }
}
