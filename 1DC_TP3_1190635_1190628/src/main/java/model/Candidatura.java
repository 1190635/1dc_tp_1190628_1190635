package model;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

public class Candidatura {

    /**
     * Representa a data da candidatura
     */
    private Date dataCandidatura;

    /**
     * Representa o valor pretendido
     */
    private double valorPretendido;

    /**
     * Representa o numero de dias
     */
    private int nrDias;

    /**
     * Representa o texto de apresentacao
     */
    private String txtApresentacao;

    /**
     * Representa o texto de motivacao
     */
    private String txtMotivacao;

    /**
     * Representa o freelancer de uma candidatura
     */
    private Freelancer freelancer;

    /**
     * Representa a tarefa
     */
    private Tarefa tarefa;

    /**
     * Construtor da classe Candidatura com todos os parametros
     */
    public Candidatura(Date dataCandidatura, double valorPretendido, int nrDias, String txtApresentacao, String txtMotivacao) {
        if ((dataCandidatura == null) || (valorPretendido < 0) || (nrDias <= 0) || (txtApresentacao == null) || (txtMotivacao == null)
                || (txtApresentacao.isEmpty()) || (txtMotivacao.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo ou vazio.");
        }
        this.dataCandidatura = dataCandidatura;
        this.valorPretendido = valorPretendido;
        this.nrDias = nrDias;
        this.txtApresentacao = txtApresentacao;
        this.txtMotivacao = txtMotivacao;
    }

    /**
     * Metodo de acesso da variavel de instancia valorPretendido
     */
    public double getValorPretendido() {
        return valorPretendido;
    }

    /**
     * Metodo de acesso da variavel de instancia dataCandidatura
     */
    public Date getDataCandidatura() {
        return dataCandidatura;
    }

    /**
     * Metodo de acesso do objeto freelancer
     */
    public Freelancer getFreelancer() {
        return freelancer;
    }

    /**
     * Metodo de acesso ao objeto tarefa
     */
    public Tarefa getTarefa() {
        return tarefa;
    }

    /**
     * Representacao textual da classe Candidatura
     */
    @Override
    public String toString() {
        return String.format("%d , %f , %d , %s , %s ", dataCandidatura, valorPretendido, nrDias, txtApresentacao, txtMotivacao);
    }

    /**
     * Metodo que calcula a media
     */
    public double calcularMediaNP(Tarefa tarefa) {
        List<CaraterCT> caraterCTS = tarefa.getCategoriaTarefa().getListaCaraterCT();
        List<Reconhecimento> reconhecimentos = freelancer.getReconhecimento();
        double soma = 0;
        int contador = 0;
        for (CaraterCT ct : caraterCTS) {
            if (ct.getIsObrigatoria()) {
                for (Reconhecimento reconhecimento : reconhecimentos) {
                    if (ct.getCompetenciaTecnica().equals(reconhecimento.getCompetenciaTecnica())) {
                        soma += reconhecimento.getGrauProficiencia().getValor();
                        contador++;
                    }
                }
            }
        }
        return soma / contador;
    }

    /**
     * Metodo que calcula o desvio padrao
     */
    public double calcularDesvioPadrao(Tarefa tarefa) {
        List<CaraterCT> caraterCTS = tarefa.getCategoriaTarefa().getListaCaraterCT();
        List<Reconhecimento> reconhecimentos = freelancer.getReconhecimento();
        List<GrauProficiencia> grausProficienciasObrigatorios = new ArrayList<GrauProficiencia>();
        for (CaraterCT ct : caraterCTS) {
            if (ct.getIsObrigatoria()) {
                for (Reconhecimento reconhecimento : reconhecimentos) {
                    if (ct.getCompetenciaTecnica().equals(reconhecimento.getCompetenciaTecnica())) {
                        grausProficienciasObrigatorios.add(reconhecimento.getGrauProficiencia());
                    }
                }
            }
        }

        if (grausProficienciasObrigatorios.size() == 1) {
            return 0;
        } else {
            double somatorio = 0;
            for (GrauProficiencia grauProficiencia : grausProficienciasObrigatorios) {
                double result = grauProficiencia.getValor() - calcularMediaNP(tarefa);
                somatorio += Math.pow(result, 2);
            }

            return Math.sqrt(somatorio / (grausProficienciasObrigatorios.size()-1));
        }
    }
}
