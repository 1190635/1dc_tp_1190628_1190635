package model;

import java.util.ArrayList;
import java.util.List;

public class RegistoAnuncios {

    /**
     * Lista pretencente a classe RegistoAnuncios que contem os anuncios
     */
    private static List<Anuncio> registoAnuncios;

    public RegistoAnuncios(){
        registoAnuncios = new ArrayList<>();
    }

    /**
     * Metodo de acesso a lista registoAnuncios
     */
    public List<Anuncio> getAnuncios(){
        return registoAnuncios;
    }

    public static boolean addAnuncio(Anuncio anuncio){
        return registoAnuncios.add(anuncio);
    }

}
