package model;

import java.util.ArrayList;
import java.util.List;

public class ListaTarefas {

    /**
     * Lista pretencente a classe ListaTarefas que contem tarefas
     */
    private static List<Tarefa> listaTarefas;

    public ListaTarefas(){
        listaTarefas = new ArrayList<>();
    }

    /**
     * Metodo que adiciona tarefas
     */
    public static boolean addTarefa(Tarefa tarefa){
        return listaTarefas.add(tarefa);
    }

    public static List<Tarefa> getListaTarefas() {
        return listaTarefas;
    }

    public void listarTarefas() {
        for (Tarefa tarefa : listaTarefas){
            System.out.println(tarefa.toString());
        }
    }
}
