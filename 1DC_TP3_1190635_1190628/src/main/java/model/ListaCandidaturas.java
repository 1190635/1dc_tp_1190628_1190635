package model;


import java.util.List;

public class ListaCandidaturas {

    /**
     * Lista pretencente a classe ListaCandidaturas que possui candidaturas
     */
    private static List<Candidatura> listaCandidaturas;

    /**
     * Metodo de acesso a lista de candidaturas
     */
    public List<Candidatura> getCandidaturas() {
        return listaCandidaturas;
    }

    /**
     * Metodo que adiciona candidaturas
     */
    public static boolean addCandidatura(Candidatura candidatura){
        return listaCandidaturas.add(candidatura);
    }
}
