package model;

public class GrauProficiencia {

    /**
     * Representa o valor
     */
    int valor;
    
    /**
     * Representa a designacao
     */
    String designacao;
    
    /**
     * Construtor da classe GrauProficiencia com todos os parametros
     */
    public GrauProficiencia(int valor, String designacao){
        if((valor <0) || (designacao == null) || (designacao.isEmpty()) ){
         throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo ou vazio.");
        }
        this.valor=valor;
        this.designacao=designacao;
    }
    
    /**
     * Metodo de acesso da variavel de instancia valor
     * @return 
     */
    public int getValor(){
        return valor;
    }
}
