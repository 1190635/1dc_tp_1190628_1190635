package model;

import java.util.List;

public class CategoriaTarefa {

    /**
     * Representa o codigo
     */
    private String codigo;

    /**
     * Representa a descricao
     */
    private String des;

    /**
     * objeto area da classe AreaAtividade
     */
    private AreaAtividade area;

    /**
     * Representa a lista de caraterCT
     */
    private List<CaraterCT> lstCaraterCT;

    /**
     * Construtor da classe CategoriaTarefa com todos os parametros
     */
    public CategoriaTarefa(String codigo, String des, AreaAtividade area) {
        if ((codigo == null) || (des == null) || (area == null) || (des.isEmpty()) || (codigo.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }
        this.codigo = codigo;
        this.des = des;
        this.area = area;
    }

    /**
     * Metodo de acesso a listCaraterCT
     */
    public List<CaraterCT> getListaCaraterCT() {
        return lstCaraterCT;
    }

    /**
     * Metodo que adiciona caraterCT a lstCaraterCT
     */
    public boolean addCaraterCt(CaraterCT caraterCT) {
        return lstCaraterCT.add(caraterCT);
    }

}
