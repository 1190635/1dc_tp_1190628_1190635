package model;

import model.ListaTarefas;
import model.Tarefa;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Scanner;

public class Startup {

    public Startup() throws FileNotFoundException {
        readTarefasFromFile();
        readAnuncioFromFile();
        readCandidaturaFromFile();
    }

    public void readAnuncioFromFile(){

        File file = new File(Constantes.ANUNCIO_PATH_NAME);
        try (Scanner in = new Scanner(file)) {

            while (in.hasNextLine()) {
                String linha = in.nextLine();
                String[] array;
                array = linha.split(";");

                Anuncio anuncio = new Anuncio(createDate(array[0]), createDate(array[1]), createDate(array[2]), createDate(array[3]), createDate(array[4]), createDate(array[5]));
                RegistoAnuncios.addAnuncio(anuncio);
            }

        }catch (FileNotFoundException fnfe){
            //CRIAR ALERTA NA INTERFACE GRAFICA
        }

    }

    public Date createDate(String data){
        return new Date(data);
    }

    public void readAreaAtividadeFromFile(){

    }

    public void readCandidaturaFromFile(){

        File file = new File(Constantes.CANDIDATURA_PATH_NAME);
        try (Scanner in = new Scanner(file)) {

            while (in.hasNextLine()) {
                String linha = in.nextLine();
                String[] array;
                array = linha.split(";");

                Candidatura candidatura = new Candidatura(createDate(array[0]), Double.parseDouble(array[1]), Integer.parseInt(array[2]), array[3], array[4]);
                ListaCandidaturas.addCandidatura(candidatura);
            }

        }catch (FileNotFoundException fnfe){
            //CRIAR ALERTA NA INTERFACE GRAFICA
        }

    }

    public void readCaregoriaTarefaFromFile(){

    }

    public void readCompetenciaTecnicaFromFile(){

    }

    public void readFreelancerFromFile(){

    }

    public void readGrauProficienciaFromFile(){

    }

    public void readListaCandidaturasFromFile(){

    }

    public void readPlataformaFromFile(){

    }

    public void readProcessoSeriacaoFromFile(){

    }

    public void readTarefasFromFile() throws FileNotFoundException {

        File file = new File(Constantes.TAREFA_PATH_NAME);
        try (Scanner in = new Scanner(file)) {

            while (in.hasNextLine()) {
                String linha = in.nextLine();
                String[] array;
                array = linha.split(";");

                Tarefa tarefa = new Tarefa(array[0], array[1], array[2], array[3], Double.parseDouble(array[4]), Double.parseDouble(array[5]));
                ListaTarefas.addTarefa(tarefa);
            }

        }catch (FileNotFoundException fnfe){
            //CRIAR ALERTA NA INTERFACE GRAFICA
        }
    }
}
