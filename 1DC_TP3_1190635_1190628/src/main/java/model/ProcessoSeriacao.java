package model;

import java.util.Date;
import java.util.List;

public class ProcessoSeriacao {

    /**
     * Representa a data de realizacao
     */
    Date dataRealizacao;
    
    /**
     * Representa o tipo de regimento aplicado ao processo de seriacao
     */
    TipoRegimento tipoRegimento;
    
    /**
     * Representa a lista de classificacoes aplicadas a um processo de seriacao
     */
    List<Classificacao> listaClassificacoes;

    public ProcessoSeriacao(TipoRegimento tipoReg){
     if(tipoReg == null){
         throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo .");
     }
     this.tipoRegimento=tipoReg;
    }

    /**
     * Metodo que adiciona a classificacao
     */
    public boolean addClassificacao(Classificacao classificacao){
        return listaClassificacoes.add(classificacao);
    }

}
