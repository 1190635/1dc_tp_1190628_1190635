/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Date;

/**
 *
 * @author ptubb
 */
public class Reconhecimento {

    /**
     * Representa a data de reconhecimento
     */
    private Date dataReconhecimento;

    /**
     * Representa a competenciaTecnica
     */
    private CompetenciaTecnica competenciaTecnica;

    /**
     * Representa o grau de proficiencia
     */
    private GrauProficiencia grauProficiencia;

    /**
     * Construtor da classe Reconhecimentos com todos os parametros
     */
    public Reconhecimento(Date dataReconhecimento, CompetenciaTecnica competenciaTecnica, GrauProficiencia grauProficiencia) {
        setDataReconhecimento(dataReconhecimento);
        setCompetenciaTecnica(competenciaTecnica);
        setGrauProficiencia(grauProficiencia);
    }

    /**
     * Metodo de acesso ao objeto competenciaTecnica
     */
    public CompetenciaTecnica getCompetenciaTecnica() {
        return competenciaTecnica;
    }

    /**
     * Metodo de acesso ao objeto grauProficiencia
     */
    public GrauProficiencia getGrauProficiencia() {
        return grauProficiencia;
    }

    /**
     * Metodo de modificacao da variavel de instancia dataReconhecimento
     */
    public void setDataReconhecimento(Date dataReconhecimento) {
        this.dataReconhecimento = dataReconhecimento;
    }

    /**
     * Metodo de modificacao da variavel de instancia competenciaTecnica
     */
    public void setCompetenciaTecnica(CompetenciaTecnica competenciaTecnica) {
        this.competenciaTecnica = competenciaTecnica;
    }

    /**
     * Metodo de modificacao do objeto grauProficiencia
     */
    public void setGrauProficiencia(GrauProficiencia grauProficiencia) {
        this.grauProficiencia = grauProficiencia;
    }

    /**
     * Representacao textual da classe Reconhecimento
     *
     * @return
     */
    @Override
    public String toString() {
        return "Data do reconhecimento=" + dataReconhecimento + "\n"
                + "Competencia T�cnica=" + competenciaTecnica
                + "Grau Profici�ncia=" + grauProficiencia + "\n";
    }
}
