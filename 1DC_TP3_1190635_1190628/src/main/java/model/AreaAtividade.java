package model;

public class AreaAtividade {

    /**
     * Representa o codigo
     */
    private String m_strCodigo;
    
    /**
     * Representa a descricao breve
     */
    private String m_strDescricaoBreve;
    
    /**
     * Representa a descricao detalhada
     */
    private String m_strDescricaoDetalhada;

/**
 * Construtor da classe AreaAtividade com todos os parametros
 */
    public AreaAtividade(String strCodigo, String strDescricaoBreve, String strDescricaoDetalhada)
    {
        if ( (strCodigo == null) || (strDescricaoBreve == null) || (strDescricaoDetalhada == null) ||
                (strCodigo.isEmpty())|| (strDescricaoBreve.isEmpty())|| (strDescricaoDetalhada.isEmpty()))
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");

        this.m_strCodigo = strCodigo;
        this.m_strDescricaoBreve = strDescricaoBreve;
        this.m_strDescricaoDetalhada = strDescricaoDetalhada;
    }

}
