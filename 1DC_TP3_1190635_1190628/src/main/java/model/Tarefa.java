package model;

public class Tarefa {

    /**
     * Representa a referencia unica por organizacao
     */
    private String refUnica;

    /**
     * Representa o nome da tarefa
     */
    private String designacao;

    /**
     * Representa a descricao informal
     */
    private String desInfor;

    /**
     * Representa a descricao tecnica
     */
    private String desTec;

    /**
     * Representa a duracao
     */
    private double duracao;

    /**
     * Representa o custo
     */
    private double custo;

    /**
     * Representa a categoria de tarefa
     */
    private CategoriaTarefa categoriaTarefa;
    
    /**
     * Construtor da classe Tarefa com todos os parametros
     */
    public Tarefa(String refUnica, String designacao, String desInfor, String desTec, double duracao, double custo) {
        if ((refUnica == null) || (designacao == null) || (desInfor == null) || (desTec == null) || (duracao <= 0) || (custo < 0)
                || (refUnica.isEmpty()) || (designacao.isEmpty()) || (desInfor.isEmpty()) || (desTec.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.refUnica = refUnica;
        this.designacao = designacao;
        this.desInfor = desInfor;
        this.desTec = desTec;
        this.duracao = duracao;
        this.custo = custo;
    }

    public CategoriaTarefa getCategoriaTarefa(){
        return categoriaTarefa;
    }
    
    @Override
    public String toString() {
        return "Tarefa{"
                + "refUnica='" + refUnica + '\''
                + ", designacao='" + designacao + '\''
                + ", desInfor='" + desInfor + '\''
                + ", desTec='" + desTec + '\''
                + ", duracao=" + duracao
                + ", custo=" + custo
                + '}';
    }
}
