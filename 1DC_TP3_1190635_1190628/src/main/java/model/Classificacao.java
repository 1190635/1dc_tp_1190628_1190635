package model;

public class Classificacao {

    /**
     * Representa a candidatura
     */
    private Candidatura cand;

    /**
     * Representa a ordem
     */
    private int ordem;

    /**
     * Construtor da classe Classificacao com todos os parametros
     */
    public Classificacao(Candidatura cand, int ordem) {
        if ((cand == null) || (ordem <= 0)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo.");
        }
        this.cand = cand;
        this.ordem = ordem;
    }

    /**
     * Representacao textual da classe Classificacao
     *
     * @return
     */
    @Override
    public String toString() {
        return "Lugar: " + ordem + "\n"
                + "Nome: " + cand.getFreelancer().getNome() + "\n"
                + "Valor pretendido: " + cand.getValorPretendido() + "�" + "\n"
                + "Data de candidatura: " + cand.getDataCandidatura() + "\n";
    }
}
