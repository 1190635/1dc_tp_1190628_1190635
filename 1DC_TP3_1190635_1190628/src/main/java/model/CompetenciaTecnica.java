package model;

public class CompetenciaTecnica {

    /**
     * Representa o codigo
     */
    String codigo;

    /**
     * Representa a descricao breve
     */
    String descBreve;

    /**
     * Representa a descricao detalhada
     */
    String descDetalhada;

    /**
     * Representa o grau de proficiencia
     */
    GrauProficiencia grauProficiencia;

    /**
     * Representa a area de atividade
     */
    AreaAtividade area;

    /**
     * Construtor da classe CompetenciaTecnica com todos os parametros
     */
    public CompetenciaTecnica(String cod, String desc_breve, String descr_det, AreaAtividade area) {
        if ((cod == null) || (desc_breve == null) || (descr_det == null)
                || (area == null) || (cod.isEmpty()) || (desc_breve.isEmpty()) || (descr_det.isEmpty())) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode ser nulo ou vazio.");
        }

        this.codigo = cod;
        this.descBreve = desc_breve;
        this.descDetalhada = descr_det;
    }

    /**
     * Metodo de acesso da variavel de instancia codigo
     *
     * @return
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo de acesso do objeto grauProficiencia
     */
    public GrauProficiencia getGrauProficiencia() {
        return grauProficiencia;
    }

    /**
     * Representacao textual da classe CompetenciaTecnica
     *
     * @return
     */
    @Override
    public String toString() {
        return "Codigo:" + codigo + "\n"
                + "Descri��o breve: " + descBreve + "\n"
                + "Descri��o detalhada: " + descDetalhada + "\n"
                + "�rea de atividade: " + area + "\n";
    }

    /**
     * Metodo que compara uma competencia tecnica com outra comepetenciaTecninca
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (outroObjeto == null || getClass() != outroObjeto.getClass()) {
            return false;
        }
        CompetenciaTecnica competenciaTecnica = (CompetenciaTecnica) outroObjeto;
        return codigo.equalsIgnoreCase(competenciaTecnica.getCodigo());
    }
}
