/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;


public class Constantes
{
    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Tarefa guardadas
     */
    public static final String TAREFA_PATH_NAME = "src\\main\\resources\\ClassFiles\\tarefas.txt";

    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Anuncio guardadas
     */
    public static final String ANUNCIO_PATH_NAME = "src\\main\\resources\\ClassFiles\\anuncios.txt";

    /**
     * Representa o caminho relativo onde se encontra o ficheiro txt com varias instancias da classe Candidatura guardadas
     */
    public static final String CANDIDATURA_PATH_NAME = "src\\main\\resources\\ClassFiles\\candidaturas.txt";
}
