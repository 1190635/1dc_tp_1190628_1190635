package controller;

import model.*;

import java.util.List;

public class SeriarAnuncioController {

    /**
     * objeto plataforma da classe Plataforma
     */
    private Plataforma plataforma;

    /**
     * objeto anu da classe Anuncio
     */
    private Anuncio anu;

    /**
     * objeto cand da classe Candidatura
     */
    private Candidatura cand;

    /**
     * objeto ps da classe ProcessoSeriacao
     */
    private ProcessoSeriacao ps;

    /**
     * objeto listaCand da classe ListaCandidaturas
     */
    private ListaCandidaturas listaCand;

    /**
     * objeto ra da classe RegistoAnuncios
     */
    private RegistoAnuncios ra;

    /**
     * Metodo de confirmacao do utilizador
     */
    public SeriarAnuncioController()
    {

    }

    /**
     * Metodo de acesso dos anuncios por seriar nao automaticos
     * @return
     */
    public List<Anuncio> getAnuncios(){

        this.ra = plataforma.getRegistoAnuncios();
        return ra.getAnuncios();
    }

    /**
     * Metodo de acesso das candidaturas
     * @param anuncioId
     * @return
     */
    public List<Candidatura> getCandidaturas(String anuncioId){

        this.listaCand = anu.getListaCandidaturas();
        this.ps = anu.novoProcessoSeriacao();

        return listaCand.getCandidaturas();
    }


    /**
     * inicia o processo de seriacao
     */
    public void iniciarSeriacao(){
       // this.cand = listaCand.getCandidatura(candId); // é necessario id?
        // ps.addClassificacao(cand,ordem);
    }

    /**
     * Regista o processo de seriacao
     */
    public void registaProcessoSeriacao(){
        anu.registaProcessoSeriacao(ps);
    }
}
