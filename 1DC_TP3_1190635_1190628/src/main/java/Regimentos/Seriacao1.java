package Regimentos;

import java.util.Comparator;
import model.Candidatura;
import model.Tarefa;
import model.TipoRegimento;

public class Seriacao1 implements TipoRegimento, Comparator<Candidatura> {
    
    @Override
    public int compare(Candidatura candidatura, Candidatura outraCandidatura) {
        Tarefa tarefa1 = candidatura.getTarefa();
        Tarefa tarefa2 = outraCandidatura.getTarefa();
        
        if ((candidatura == null) || (outraCandidatura == null)) {
            throw new IllegalArgumentException("Nenhum dos argumentos pode  ser nulo .");
        }
        int comparador = Double.compare(candidatura.calcularMediaNP(tarefa1), outraCandidatura.calcularMediaNP(tarefa2));
        if (comparador == 0) {
            comparador = Double.compare(candidatura.getValorPretendido(), outraCandidatura.getValorPretendido());
            if (comparador == 0) {
                comparador = candidatura.getDataCandidatura().compareTo(candidatura.getDataCandidatura());
            }
        }
        return comparador;
    }
    
}
