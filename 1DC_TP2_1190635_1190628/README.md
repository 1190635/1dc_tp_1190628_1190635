# README #

### What is this repository for? ###

Repository for the practical evaluation #1 done by Gon�alo Carvalho and Gon�alo Ferreira

### Work Methodology ###

Work was done via audio call and screen share, so all commits are shared by both members of the group.

### Class diagram of the Maven Project ###

![class diagram](UML/tp1_uml.png)