/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dei.tp1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gon�alo
 */
public class CreditoEducacaoTest {
    
    public CreditoEducacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of CalcularMontanteAReceberPorCadaCredito method, of class CreditoEducacao.
     */
    @Test
    public void testCalcularMontanteAReceberPorCadaCredito() {
        System.out.println("CalcularMontanteAReceberPorCadaCredito");
        CreditoEducacao instance = new CreditoEducacao("goncalo","engenheiro",18000,60,24);
        double expResult = 19275.0;
        double result = instance.CalcularMontanteAReceberPorCadaCredito();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of CalcularMontanteTotalJuros method, of class CreditoEducacao.
     */
    @Test
    public void testCalcularMontanteTotalJuros() {
        System.out.println("CalcularMontanteTotalJuros");
        CreditoEducacao instance = new CreditoEducacao("goncalo","engenheiro",18000,60,24);
        double expResult = 1275.0;
        double result = instance.CalcularMontanteTotalJuros();
        assertEquals(expResult, result, 0.0);
    }
    
}
