/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dei.tp1;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Gon�alo
 */
public class CreditoHabitacaoTest {
    
    public CreditoHabitacaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of CalcularMontanteAReceberPorCadaCredito method, of class CreditoHabitacao.
     */
    @Test
    public void testCalcularMontanteAReceberPorCadaCredito() {
        System.out.println("CalcularMontanteAReceberPorCadaCredito");
        CreditoHabitacao instance = new CreditoHabitacao("zezoca","entretenimento",120000,240,1);
        double expResult = 133255.0;
        double result = instance.CalcularMontanteAReceberPorCadaCredito();
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of CalcularMontanteTotalJuros method, of class CreditoHabitacao.
     */
    @Test
    public void testCalcularMontanteTotalJuros() {
        System.out.println("CalcularMontanteTotalJuros");
        CreditoHabitacao instance = new CreditoHabitacao("zezoca","entretenimento",120000,240,1);
        double expResult = 13255.0;
        double result = instance.CalcularMontanteTotalJuros();
        assertEquals(expResult, result, 0.0);
    }
    
}
