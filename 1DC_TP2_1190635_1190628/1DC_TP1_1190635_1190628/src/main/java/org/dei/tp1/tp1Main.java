package org.dei.tp1;

public class tp1Main {

    public static void main(String[] args) {

        //a)
            CreditoHabitacao ch1 = new CreditoHabitacao("Goncalo Carvalho","informatico",120000,240,1);
            CreditoHabitacao ch2 = new CreditoHabitacao("Goncalo Ferreira","informatico",20000,300,3);

            CreditoAutomovel ca1 = new CreditoAutomovel("Zeze","Futebolista",35000,400);
            CreditoAutomovel ca2 = new CreditoAutomovel("Ricardo","Humorista",20000,250);

            CreditoEducacao ce1 = new CreditoEducacao("Paulo","Mecanico",18000,60,24);
            CreditoEducacao ce2 = new CreditoEducacao("Joao","Medico",34200,80,10);


         //b)
            CreditoBancario[] array = new CreditoBancario[6];
                array[0]=ch1;
                array[1]=ch2;
                array[2]=ca1;
                array[3]=ca2;
                array[4]=ce1;
                array[5]=ce2;

         //c)
        System.out.printf("\n|        Nome        | Valor Recebido pela instituiçao bancaria |\n"
                + "|====================|==========================================|\n");
        for (int b = 0; b < array.length; b++) {
            System.out.printf("|%-20s|%42.2f|\n",
                    array[b].getNome(),array[b].CalcularMontanteAReceberPorCadaCredito());
        }

        System.out.printf("\n|        Nome        | Montante do emprestimo | Valor de juros recebido |\n"
                + "|====================|========================|=========================|\n");
        for (int b = 0; b < array.length; b++) {
            System.out.printf("|%-20s|%24.0f|%25.2f|\n",
                    array[b].getNome(),array[b].getMontante(),array[b].CalcularMontanteTotalJuros());
        }

        //d)
        System.out.println();
        System.out.println("Quantidade de instancias de Creditos de Habitacao: " +  CreditoHabitacao.getContandorInstancia());
        System.out.println("Quantidade de instancias de Creditos de Consumo: " + CreditoConsumo.getContandorInstancia());

        //e)
            float total=0; float jurosTotal=0;

            for(int i=0;i<array.length;i++){
                total+=array[i].CalcularMontanteAReceberPorCadaCredito();
                jurosTotal+= array[i].CalcularMontanteTotalJuros();
            }

        System.out.println();
        System.out.println("Valor total que a instituiçao ira receber por todos os creditos criados: " + total);
        System.out.println("Valor total que a instituiçao ira receber dos juros de todos os creditos criados: " + jurosTotal);
    }
}
