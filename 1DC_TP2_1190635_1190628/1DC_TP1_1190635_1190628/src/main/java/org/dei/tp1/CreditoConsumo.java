package org.dei.tp1;

public abstract class CreditoConsumo extends CreditoBancario {

    private static int contandorInstancia=0;

    /**
     * Construtor da classe CreditoConsumo sem parametros
     */
    public CreditoConsumo(){

    }

    /**
     *Construtor da classe CreditoConsumo com todos os parametros
     */
    public CreditoConsumo(String nome, String profissao, double montante, int prazoFinanciamento){
        super(nome, profissao, montante, prazoFinanciamento);
    }

    /**
     * Metodo de modificacao da variavel de classe contadorInstancia
     * @param contandorInstancia valor a atribuir a varivavel contadorInstancia
     */
    public static void setContandorInstancia(int contandorInstancia) {
        CreditoConsumo.contandorInstancia = contandorInstancia;
    }

    /**
     * Metodo de acesso da variavel de classe contadorInstancia
     */
    public static int getContandorInstancia() {
        return contandorInstancia;
    }
}
