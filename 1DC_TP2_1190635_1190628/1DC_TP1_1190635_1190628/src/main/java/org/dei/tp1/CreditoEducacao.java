package org.dei.tp1;

public class CreditoEducacao extends CreditoConsumo{

    private static double taxaJuroAnual = 2;
    private int periodoCarencia;

    /**
     *  construtor da classe CreditoEducacao
     *  */
    public CreditoEducacao(){

        setContandorInstancia(1+getContandorInstancia());
    }

    /**
     * construtor da classe CreditoEducacao com todos os paremetros
     */
    public CreditoEducacao(String nome, String profissao, double montante, int prazoFinanciamento,int periodoCarencia){
        super(nome, profissao, montante, prazoFinanciamento);
        this.periodoCarencia=periodoCarencia;

        setContandorInstancia(1+getContandorInstancia());
    }

    /**
     * Metodo de modificacao da variavel de instancia taxaJuroAnual
     * @param taxaJuroAnual valor a atribuir a variavel taxaJuroAnual
     */
    public void setTaxaJuroAnual(double taxaJuroAnual) {
        this.taxaJuroAnual = taxaJuroAnual;
    }

    /**
     * Metodo de modificacao da variavel de instancia periodoCarencia
     * @param periodoCarencia valor a atribuir a variavel periodoCarencia
     */
    public void setPeriodoCarencia(int periodoCarencia) {
        this.periodoCarencia = periodoCarencia;
    }

    /**
     * Metodo de acesso a variavel de instancia taxaJuroAnual
     */
    public double getTaxaJuroAnual() {
        return taxaJuroAnual;
    }

    /**
     * Metodo de acesso a variavel de instancia periodoCarencia
     * @return
     */
    public int getPeriodoCarencia() {
        return periodoCarencia;
    }

    /**
     * Representa o objeto e suas respetivas variaveis em forma de String
     * @return  String representativa do objeto
     */
    @Override
    public String toString() {
        return super.toString()+
                "[Taxa de Juro Anual=" + taxaJuroAnual + "% ] "+
                "[Periodo de Carencia=" + getPeriodoCarencia() + "] ";
    }

    /**
     * Calcula o valor total, juros inclusivos, a ser pago pelo credito que invoca
     * @return  valor total
     */
    public double CalcularMontanteAReceberPorCadaCredito(){
        return getMontante()+CalcularMontanteTotalJuros();
    }

    /**
     * Calcula os juros totais a receber por um credito
     * @return valor total de juros
     */
    public double CalcularMontanteTotalJuros(){
        int periodNormal= getPrazoFinanciamento()-getPeriodoCarencia();
        double dividaRestante=getMontante();

        double total = ( ((taxaJuroAnual/100)/12)*getMontante()) * getPeriodoCarencia();

        for(int i=0;i<periodNormal;i++){
            total+=( ((taxaJuroAnual/100)/12)*dividaRestante );
            dividaRestante-=(this.getMontante()/periodNormal);
        }

        return total;
    }
}
