package org.dei.tp1;

public class CreditoAutomovel extends CreditoConsumo{

    private static double taxaDesconto = 1;
    private static double taxaJuroAnual = 6;
    private static int limiteSuperior=24;

    /**
     * Contrutor da classe CreditoAutomovel sem parametros
     */
    public CreditoAutomovel(){
        setContandorInstancia(1+getContandorInstancia());
    }

    /**
     * Construtor da classe CreditoAutomovel com todos os parametros
     */
    public CreditoAutomovel(String nome, String profissao, double montante, int prazoFinanciamento){
        super(nome, profissao, montante, prazoFinanciamento);

        setContandorInstancia(1+getContandorInstancia());
    }

    /**
     * Metodo de modificacao da variavel de instancia limiteSuperior
     * @param limiteSuperior valor a atribuir a varivavel limiteSuperior
     */
    public static void setLimiteSuperior(int limiteSuperior) {
        limiteSuperior = limiteSuperior;
    }

    /**
     * Metodo de acesso da variavel de instancia limiteSuperior
     */
    public int getLimiteSuperior() {
        return limiteSuperior;
    }

    /**
     * Metodo de acesso da variavel de instancia taxaDesconto
     */
    public double getTaxaDesconto() {
        return taxaDesconto;
    }

    /**
     * Metodo de acesso da variavel de instancia taxaJuroAnual
     */
    public double getTaxaJuroAnual() {
        return taxaJuroAnual;
    }

    /**
     * Metodo de modificacao da variavel de instancia taxaDesconto
     * @param taxaDesconto valor a atribuir a varivavel taxaDesconto
     */
    public void setTaxaDesconto(double taxaDesconto) {
        this.taxaDesconto = taxaDesconto;
    }

    /**
     * Metodo de modificacao da variavel de instancia taxaJuroAnual
     * @param taxaJuroAnual valor a atribuir a varivavel taxaJuroAnual
     */
    public void setTaxaJuroAnual(double taxaJuroAnual) {
        this.taxaJuroAnual = taxaJuroAnual;
    }

    /**
     * Calcula o valor total, juros inclusivos, a ser pago pelo credito que invoca
     * @return  valor total pago
     */
    public double CalcularMontanteAReceberPorCadaCredito(){
        if (getPrazoFinanciamento() <= getLimiteSuperior()) {
            return  ( 1-(getTaxaDesconto() / 100) ) * (getMontante() + CalcularMontanteTotalJuros());
        }else{
            return ( getMontante() + CalcularMontanteTotalJuros() );
        }
    }

    /**
     * Calcula os juros totais a receber por um credito
     * @return valor total de juros
     */
    public double CalcularMontanteTotalJuros() {
        double total = 0;
        double dividaRestante = getMontante();


            for (int i = 0; i < getPrazoFinanciamento(); i++) {
                total += (((getTaxaJuroAnual() / 100) / 12) * dividaRestante);
                dividaRestante -= (this.getMontante() / getPrazoFinanciamento());
            }

        return total;
    }

    /**
     * Representa o objeto e suas respetivas variaveis em forma de String
     * @return  String representativa do objeto
     */
    @Override
    public String toString() {
        return super.toString()+
                "[Taxa de Juro anual=" + getTaxaDesconto() + "% ] "+
                "[Desconto=" + getTaxaDesconto() + "% ] ";
    }
}
