package org.dei.tp1;

public class CreditoHabitacao extends CreditoBancario {

    private double spread;
    private static double taxaEuribor = 0.1;
    private static int contandorInstancia=0;

    /**
     * construtor da classe CreditoHabitacao com todos os parametros
     */
    public CreditoHabitacao(String nome, String profissao, double montante, int prazoFinanciamento, double spread){
        super(nome,profissao,montante,prazoFinanciamento);
        this.spread=spread;

        setContandorInstancia(1+getContandorInstancia());
    }

    /**
     * construtor da classe CreditoHabitacao sem parametros
     */

    public CreditoHabitacao(){
        setContandorInstancia(1+getContandorInstancia());
    }

    /**
     * Metodo de modificacao da variavel de instancia contadorInstancia
     * @param valor valor a atribuir a variavel contadorInstancia
     */
    public static void setContandorInstancia(int valor) {
        contandorInstancia = valor;
    }

    /**
     * Metodo de acesso da variavel de instancia contadorInstancia
     */

    public static int getContandorInstancia() {
        return contandorInstancia;
    }

    /**
     * Metodo de acesso da variavel de instancia spread
     */
    public double getSpread() {
        return spread;
    }

    /**
     * Metodo de modificacao da variavel de instancia spread
     * @param spread valor a atribuir a variavel spread
     */
    public void setSpread(double spread) {
        this.spread = spread;
    }

    /**
     * Metodo de acesso da variavel de instancia taxaEuribor
     */
    public double getTaxaEuribor() {
        return taxaEuribor;
    }

    /**
     * Metodo de modificacao da variavel de instancia taxaEuribor
     * @param taxaEuribor valor a atribuir a variavel taxaEuribor
     */
    public void setTaxaEuribor(double taxaEuribor) {
        this.taxaEuribor = taxaEuribor;
    }


    /**
     * Calcula o valor total, juros inclusivos, a ser pago pelo credito que invoca
     * @return  valor total
     */

    public double CalcularMontanteAReceberPorCadaCredito(){
        return getMontante()+CalcularMontanteTotalJuros();
    }


    /**
     * Calcula os juros totais a receber por um credito
     * @return valor total de juros
     */
    public double CalcularMontanteTotalJuros(){
        double juros=( ((getTaxaEuribor()/100)/12) + ((getSpread()/100)/12) );
        double total=0;
        double dividaRestante=getMontante();

        for(int i=0;i<getPrazoFinanciamento();i++){
            total+= (juros*dividaRestante);
            dividaRestante-=(this.getMontante()/getPrazoFinanciamento());
        }

        return total;
    }

    /**
     * Representa o objeto e suas respetivas variaveis em forma de String
     * @return  String representativa do objeto
     */
    @Override
    public String toString() {
        return super.toString()+
                "[spread=" + spread + "% ] "+
                "[taxaEuribor=" + taxaEuribor + "% ] ";
    }
}
