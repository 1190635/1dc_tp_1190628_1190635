package org.dei.tp1;

public abstract class CreditoBancario {

    private String nome;
    private String profissao;
    private double montante;
    private int prazoFinanciamento;

    /**
     * construtor da classe CreditoBancario sem parametros
     */
    public CreditoBancario(){

    }

    /**
     * construtor da classe CreditoBancario com todos os parametros
     */
    public CreditoBancario(String nome, String profissao, double montante, int prazoFinanciamento){
        this.nome=nome;
        this.profissao=profissao;
        this.montante=montante;
        this.prazoFinanciamento=prazoFinanciamento;
    }

    /**
     * Metodo de acesso a variavel de instancia montante
     */
    public double getMontante() {
        return montante;
    }

    /**
     * Metodo de acesso a variavel de instancia prazoFinanciamento
     */
    public int getPrazoFinanciamento() {
        return prazoFinanciamento;
    }

    /**
     * Metodo de acesso a variavel de instancia nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * Metodo de acesso a variavel de instancia profissao
     */
    public String getProfissao() {
        return profissao;
    }

    /**
     * Metodo de modificacao da variavel de instancia montante
     * @param montante valor a atribuir a variavel montante
     */
    public void setMontante(double montante) {
        this.montante = montante;
    }

    /**
     * Metodo de modificacao da variavel de instancia nome
     * @param nome valor a atribuir a variavel nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * Metodo de modificacao da variavel de instancia prazoFinanciamento
     * @param prazoFinanciamento valor a atribuir a variavel prazoFinanciamento
     */
    public void setPrazoFinanciamento(int prazoFinanciamento) {
        this.prazoFinanciamento = prazoFinanciamento;
    }
    /**
     * Metodo de modificacao da variavel de instancia profissao
     * @param profissao valor a atribuir a variavel profissao
     */
    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    /**
     * Metodo abstrato que obriga a sua implementacao nas subclasses
     */
    public abstract double CalcularMontanteAReceberPorCadaCredito();

    /**
     * Metodo abstrato que obriga a sua implementacao nas subclasses
     */
    public abstract double CalcularMontanteTotalJuros();

    /**
     * Representa o objeto e suas respetivas variaveis em forma de String
     * @return  String representativa do objeto
     */
    @Override
    public String toString() {
        return getClass().getSimpleName()+
                "-[nome=" + nome + "] " +
                "[profissao=" + profissao + "] " +
                "[montante=" + montante + "] "+
                "[prazoFinanciamento=" + prazoFinanciamento +
                "] ";
    }
}
