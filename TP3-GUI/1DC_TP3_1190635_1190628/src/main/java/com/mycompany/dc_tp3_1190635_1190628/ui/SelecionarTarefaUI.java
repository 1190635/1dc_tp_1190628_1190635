/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dc_tp3_1190635_1190628.ui;

import com.mycompany.dc_tp3_1190635_1190628.controller.SeriarAnuncioController;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Gonçalo
 */
public class SelecionarTarefaUI implements Initializable{

    @FXML
    private Label lblSelecionarTarefa;
    @FXML
    private Button btnConfirmarSelecaoTarefa;
    @FXML
    private ListView<?> lstViewTarefas;
    
    private SeriarAnuncioController appController;
    private Stage processoSeriacaoStage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/ProcessoSeriacaoUI.fxml"));
            Parent root = loader.load();
            
            Scene scene = new Scene(root);
            
            processoSeriacaoStage = new Stage();
            processoSeriacaoStage.initModality(Modality.APPLICATION_MODAL);
            processoSeriacaoStage.setTitle("Anúncio em seriação");
            processoSeriacaoStage.setResizable(false);
            processoSeriacaoStage.setScene(scene);
            
            ProcessoSeriacaoUI processoSeriacaoUI = loader.getController();
            processoSeriacaoUI.associarParentUI(this);
        } catch (IOException ex) {
            AlertaUI.criarAlerta(Alert.AlertType.ERROR, MainApp.TITULO_APLICACAO, "Erro.", ex.getMessage());
        }
    }

    public SeriarAnuncioController getAplicacaoController() {
        return appController;
    }
    
    public void atualizaListViewTarefas() {
        // lstViewTarefas.setItems(FXCollections.observableArrayList(appController.getTarefas()));
    }

    @FXML
    private void confirmarTarefaAction(ActionEvent event) {
        btnConfirmarSelecaoTarefa.setDisable(false);
    }

    @FXML
    private void checkSelectionAction(MouseEvent event) {
        if(lstViewTarefas.getSelectionModel().getSelectedItem()!= null){
            btnConfirmarSelecaoTarefa.setDisable(false);
        }
    }
    
}
