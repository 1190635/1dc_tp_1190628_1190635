/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.dc_tp3_1190635_1190628.ui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

/**
 *
 * @author Gonçalo
 */
public class ProcessoSeriacaoUI implements Initializable {

    @FXML
    private Label lblSeriarAnuncioPergunta;
    @FXML
    private Button btnConfirmarInicioSeriacao;
    @FXML
    private Button btnCancelarSeriacao;
    @FXML
    private ListView<?> lstViewCandidaturas;
    
    private SelecionarTarefaUI selecionarTarefaUI;
    
    public void associarParentUI(SelecionarTarefaUI selecionarTarefaUI) {
        this.selecionarTarefaUI = selecionarTarefaUI;
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @FXML
    private void ConfirmarInicioSeriacaoAction(ActionEvent event) {
    }

    @FXML
    private void cancelarSeriacaoAction(ActionEvent event) {
    }

    
    
}
