/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tp3_gui_2.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Gonçalo
 */
public class SelecionarTarefaUI implements Initializable{

    @FXML
    private Label lblSelecionarTarefa;
    @FXML
    private Button btnConfirmarSelecaoTarefa;
    @FXML
    private ListView<?> lstViewTarefas;
    
    private Stage processoSeriacaoStage;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }
    
    public void atualizaListViewTarefas() {
        // lstViewTarefas.setItems(FXCollections.observableArrayList(appController.getTarefas()));
    }

    @FXML
    private void confirmarTarefaAction(ActionEvent event) {
        btnConfirmarSelecaoTarefa.setDisable(false);
    }

    @FXML
    private void checkSelectionAction(MouseEvent event) {
        if(lstViewTarefas.getSelectionModel().getSelectedItem()!= null){
            btnConfirmarSelecaoTarefa.setDisable(false);
        }
    }
    
}
